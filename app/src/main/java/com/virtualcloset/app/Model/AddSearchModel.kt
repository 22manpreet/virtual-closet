package com.virtualcloset.app.Model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class AddSearchModel(

	@field:SerializedName("data")
	val data: ArrayList<SearchDataItem?>? = null,

	@field:SerializedName("details")
	val details: ArrayList<DetailsItem?>? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: Int? = null
) : Parcelable

@Parcelize
data class SearchDataItem(

	@field:SerializedName("search")
	val search: String? = null,

	@field:SerializedName("user_id")
	val userId: String? = null,

	@field:SerializedName("creation_at")
	val creationAt: String? = null,

	@field:SerializedName("count")
	val count: String? = null,

	@field:SerializedName("update_at")
	val updateAt: String? = null,

	@field:SerializedName("search_id")
	val searchId: String? = null
) : Parcelable

@Parcelize
data class DetailsItem(

	@field:SerializedName("image5")
	val image5: String? = null,

	@field:SerializedName("image3")
	val image3: String? = null,

	@field:SerializedName("image4")
	val image4: String? = null,

	@field:SerializedName("category_id")
	val categoryId: String? = null,

	@field:SerializedName("item_id")
	val itemId: String? = null,

	@field:SerializedName("user_id")
	val userId: String? = null,

	@field:SerializedName("creation_at")
	val creationAt: String? = null,

	@field:SerializedName("description")
	val description: String? = null,

	@field:SerializedName("image1")
	val image1: String? = null,

	@field:SerializedName("image2")
	val image2: String? = null
) : Parcelable

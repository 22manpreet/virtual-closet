package com.virtualcloset.app.Model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class SubscriptionStatusModel(
	val message: String? = null,
	val status: Int? = null
) : Parcelable

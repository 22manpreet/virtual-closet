package com.virtualcloset.app.Model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class EditProfileModel(

	@field:SerializedName("data")
	val data: EditProfileData? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: Int? = null
) : Parcelable

@Parcelize
data class EditProfileData(

	@field:SerializedName("image")
	val image: String? = null,

	@field:SerializedName("password")
	val password: String? = null,

	@field:SerializedName("device_name")
	val deviceName: String? = null,

	@field:SerializedName("user_id")
	val userId: String? = null,

	@field:SerializedName("email_verification")
	val emailVerification: String? = null,

	@field:SerializedName("creation_at")
	val creationAt: String? = null,

	@field:SerializedName("disable")
	val disable: String? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("authtoken")
	val authtoken: String? = null,

	@field:SerializedName("verification_code")
	val verificationCode: String? = null,

	@field:SerializedName("email")
	val email: String? = null
) : Parcelable

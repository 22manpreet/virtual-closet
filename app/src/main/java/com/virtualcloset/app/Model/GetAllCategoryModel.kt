package com.virtualcloset.app.Model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class GetAllCategoryModel(

	@field:SerializedName("data")
	val data: ArrayList<CatgoryDataItem?>? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: Int? = null
) : Parcelable

@Parcelize
data class CatgoryDataItem(

	@field:SerializedName("category_name")
	val categoryName: String? = null,

	@field:SerializedName("role")
	val role: String? = null,

	@field:SerializedName("category_id")
	val categoryId: String? = null,

	@field:SerializedName("user_id")
	val userId: String? = null,

	@field:SerializedName("creation_at")
	val creationAt: String? = null,

	@field:SerializedName("is_remove")
	val isRemove: String? = null
) : Parcelable

package com.virtualcloset.app.Model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class AddItemsModel(

	@field:SerializedName("data")
	val data: AddItemData? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: Int? = null
) : Parcelable

@Parcelize
data class AddItemData(

	@field:SerializedName("image5")
	val image5: String? = null,

	@field:SerializedName("image3")
	val image3: String? = null,

	@field:SerializedName("image4")
	val image4: String? = null,

	@field:SerializedName("category_id")
	val categoryId: String? = null,

	@field:SerializedName("item_id")
	val itemId: String? = null,

	@field:SerializedName("user_id")
	val userId: String? = null,

	@field:SerializedName("creation_at")
	val creationAt: String? = null,

	@field:SerializedName("description")
	val description: String? = null,

	@field:SerializedName("image1")
	val image1: String? = null,

	@field:SerializedName("image2")
	val image2: String? = null
) : Parcelable

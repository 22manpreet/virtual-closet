package com.virtualcloset.app.Model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class GetProfileModel(
	val data: GetProfileData? = null,
	val message: String? = null,
	val status: Int? = null
) : Parcelable

@Parcelize
data class GetProfileData(
	val image: String? = null,
	val password: String? = null,
	val deviceName: String? = null,
	val userId: String? = null,
	val emailVerification: String? = null,
	val creationAt: String? = null,
	val disable: String? = null,
	val name: String? = null,
	val authtoken: String? = null,
	val verificationCode: String? = null,
	val email: String? = null
) : Parcelable

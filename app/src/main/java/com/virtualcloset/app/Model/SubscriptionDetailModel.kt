package com.virtualcloset.app.Model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class SubscriptionDetailModel(

    @field:SerializedName("expires_date")
    var expiresDate: String? = "",

    @field:SerializedName("original_transaction_id")
    var originalTransactionId: String? = "",

    @field:SerializedName("transaction_id")
    var transactionId: String? = "",

    @field:SerializedName("product_id")
    var productId: String? = "",

) : Parcelable
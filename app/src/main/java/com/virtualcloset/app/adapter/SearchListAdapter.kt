package com.virtualcloset.app.adapter

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.virtualcloset.app.Interface.SearchItemClickListener
import com.virtualcloset.app.Model.SearchDataItem
import com.virtualcloset.app.R

class SearchListAdapter(
    var mActivity: Activity,
    var AddSearchArrayList: ArrayList<SearchDataItem?>?,
    var mSearchItemClickListener: SearchItemClickListener
): RecyclerView.Adapter<SearchListAdapter.ViewHolder>() {
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): SearchListAdapter.ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val view: View = layoutInflater.inflate(R.layout.search_catgory_list, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: SearchListAdapter.ViewHolder, position: Int) {
        holder.searchTV.setText(AddSearchArrayList!!.get(position)!!.search)


        holder.itemView.setOnClickListener {
            mSearchItemClickListener.onItemClickListenr(AddSearchArrayList!!.get(position)!!.search)
        }
    }

    override fun getItemCount(): Int {
      return AddSearchArrayList!!.size
    }
    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
         var searchTV:TextView
        init {
            searchTV= itemView.findViewById(R.id.searchTV)
        }
    }
}
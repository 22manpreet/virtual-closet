package com.virtualcloset.app.adapter

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.virtualcloset.app.Model.DataItem
import com.virtualcloset.app.Model.ItemDetailsItem
import com.virtualcloset.app.R
import com.virtualcloset.app.activity.FavSeeMore
import com.virtualcloset.app.activity.SeeMoreActivity

class AllFavVerticalListAdapter(var activity: FragmentActivity, var allItemsArrayList: ArrayList<DataItem?>?) : RecyclerView.Adapter<AllFavVerticalListAdapter.ViewHolder>(){

    var allHorizontalListAdapter: AllHorizontalListAdapter? = null

    var itemDetailsArrayList: ArrayList<ItemDetailsItem?>? = ArrayList()
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): AllFavVerticalListAdapter.ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val view: View = layoutInflater.inflate(R.layout.all_vertical_list, parent, false)
        return ViewHolder(view)
    }
    override fun onBindViewHolder(holder: AllFavVerticalListAdapter.ViewHolder, position: Int) {

        var myString:String?=allItemsArrayList!!.get(position)!!.categoryName
        holder.categoryTV.setText(myString!!.substring(0, 1).toUpperCase() + myString.substring(1).toLowerCase())


        itemDetailsArrayList= allItemsArrayList!!.get(position)!!.itemDetails

        val layoutManager: RecyclerView.LayoutManager = LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false)
        holder.horizontalRV.setLayoutManager(layoutManager)
        allHorizontalListAdapter = AllHorizontalListAdapter(activity,itemDetailsArrayList,allItemsArrayList!!.get(position)!!.categoryName)
        holder.horizontalRV.setAdapter(allHorizontalListAdapter)

        if(itemDetailsArrayList!!.size > 4){
            holder.see_moreTV.visibility=View.VISIBLE
        }
        else{
            holder.see_moreTV.visibility=View.GONE
        }
        holder.see_moreTV.setOnClickListener {
            var i= Intent(activity, FavSeeMore::class.java)
            i.putExtra("category_id",allItemsArrayList!!.get(position)!!.categoryId)
            i.putExtra("category_name",allItemsArrayList!!.get(position)!!.categoryName)
            activity.startActivity(i)
        }
    }

    override fun getItemCount(): Int {
        return allItemsArrayList!!.size
    }
    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var horizontalRV: RecyclerView
        var categoryTV : TextView
        var see_moreTV : TextView
        init {
            horizontalRV= itemView.findViewById(R.id.horizontalRV)
            categoryTV = itemView.findViewById(R.id.categoryTV)
            see_moreTV = itemView.findViewById(R.id.see_moreTV)
        }
    }
}
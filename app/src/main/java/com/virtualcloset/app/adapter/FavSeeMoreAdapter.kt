package com.virtualcloset.app.adapter

import android.app.Activity
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.virtualcloset.app.Interface.PaginationInquiriesInterface
import com.virtualcloset.app.Model.FavDetailDataItem
import com.virtualcloset.app.R

class FavSeeMoreAdapter(
    var mActivity: Activity,
    var favitemDetailsArrayList: ArrayList<FavDetailDataItem?>?,
    var mInterfaceData: PaginationInquiriesInterface?
) : BaseAdapter(){
    private var layoutInflater: LayoutInflater? = null
    private lateinit var favTV: TextView
    private lateinit var imageIV: ImageView

    override fun getCount(): Int {
        return favitemDetailsArrayList!!.size
    }

    override fun getItem(position: Int): Any? {
        return null
    }

    override fun getItemId(position: Int): Long {
        return 0
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        var convertView = convertView

        if (position >= favitemDetailsArrayList!!.size - 1) {
            mInterfaceData!!.mPaginationInquiriesInterface(true)
        }

        if (layoutInflater == null) {
            layoutInflater = mActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        }
        if (convertView == null) {
            convertView = layoutInflater!!.inflate(R.layout.rowitem, null)
        }
        favTV = convertView!!.findViewById(R.id.favTV)
        imageIV = convertView!!.findViewById(R.id.imageIV)
        if((favitemDetailsArrayList!!.get(position)!!.isFav) .equals("1")){
            favTV.setBackgroundResource(R.drawable.ic_fav)
        }
        else if((favitemDetailsArrayList!!.get(position)!!.isFav) .equals("2")){
            favTV.setBackgroundResource(R.drawable.ic_unfav)
        }
        if(favitemDetailsArrayList!!.get(position)!= null){
            Glide.with(mActivity)
                .load(favitemDetailsArrayList!!.get(position)!!.image1)
                .placeholder(R.drawable.ic_placeholder_image)
                .error(R.drawable.ic_placeholder_image)
                .into(imageIV)
        }
//        if (itemDetailsArrayList!!.size % 10 == 0 && itemDetailsArrayList!!.size - 1 == position){
//            mLoadMoreScrollListner.onLoadMoreListner(itemDetailsArrayList!!.get(position))
//        }
        return convertView
    }
}
package com.virtualcloset.app.adapter

import android.app.Activity
import android.graphics.BitmapFactory
import android.os.SystemClock
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.virtualcloset.app.Interface.RemoveImageInterface
import com.virtualcloset.app.Model.ImagesModel
import com.virtualcloset.app.R

class ImageListAdapter(
    var mActivity: Activity,
    var imagesArrayList: ArrayList<ImagesModel>,
    var mRemoveImageInterface: RemoveImageInterface
): RecyclerView.Adapter<ImageListAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ImageListAdapter.ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val view: View = layoutInflater.inflate(R.layout.images_list, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ImageListAdapter.ViewHolder, position: Int) {
        val myModel: ImagesModel = imagesArrayList.get(position)
        val bitmap = BitmapFactory.decodeFile(myModel.image_name)
        holder.imageIV.setImageBitmap(bitmap)

        //  Glide.with(context).load(bitmap).override(500, 300).into(holder.uploadImg);
        holder.CrossIV.setOnClickListener(View.OnClickListener {
            var mLastClickTime: Long = 0
            // mis-clicking prevention, using threshold of 1000 ms
            if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                return@OnClickListener
            }
            mLastClickTime = SystemClock.elapsedRealtime()
            mRemoveImageInterface.onRemoveClick(position, "changedImages")
        })
    }

    override fun getItemCount(): Int {
       return imagesArrayList!!.size
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var imageIV: ImageView
        var CrossIV: TextView
        init {
            imageIV= itemView.findViewById(R.id.imageIV)
            CrossIV= itemView.findViewById(R.id.CrossIV)
        }
    }
}
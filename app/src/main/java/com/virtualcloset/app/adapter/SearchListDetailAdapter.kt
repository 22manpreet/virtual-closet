package com.virtualcloset.app.adapter

import android.app.Activity
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.virtualcloset.app.Model.GetSearchDataItem
import com.virtualcloset.app.R
import com.virtualcloset.app.activity.DetailActivity

class SearchListDetailAdapter(
    var mActivity: Activity,
    var GetDetailArrayList: ArrayList<GetSearchDataItem?>?
): RecyclerView.Adapter<SearchListDetailAdapter.ViewHolder>()  {
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): SearchListDetailAdapter.ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val view: View = layoutInflater.inflate(R.layout.search_detail_list, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: SearchListDetailAdapter.ViewHolder, position: Int) {
        if(GetDetailArrayList!!.get(position)!!.image1 != null) {
            Glide.with(mActivity)
                .load(GetDetailArrayList!!.get(position)!!.image1)
                .placeholder(R.drawable.ic_placeholder_image)
                .error(R.drawable.ic_placeholder_image)
                .into(holder.imageIV)
        }

        if(GetDetailArrayList!!.get(position)!!.isFav.equals("1")){
            holder.favTV.setBackgroundResource(R.drawable.ic_fav)
        }
        else if(GetDetailArrayList!!.get(position)!!.isFav.equals("2")){
            holder.favTV.setBackgroundResource(R.drawable.ic_unfav)
        }

        holder.itemView.setOnClickListener {
            val intent = Intent(mActivity, DetailActivity::class.java)
            intent.putExtra("category_name", "")
            intent.putExtra("item_id",GetDetailArrayList!!.get(position)!!.itemId)
            mActivity.startActivity(intent)
        }
    }

    override fun getItemCount(): Int {
       return GetDetailArrayList!!.size
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
       var imageIV : ImageView
       var favTV : TextView
        init {
            imageIV = itemView.findViewById(R.id.imageIV)
            favTV =itemView.findViewById(R.id.favTV)
        }
    }
}
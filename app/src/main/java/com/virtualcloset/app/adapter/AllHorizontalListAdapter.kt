package com.virtualcloset.app.adapter

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.virtualcloset.app.Model.ItemDetailsItem
import com.virtualcloset.app.R
import com.virtualcloset.app.activity.DetailActivity

class AllHorizontalListAdapter(
    var activity: FragmentActivity,
    var itemDetailsArrayList: ArrayList<ItemDetailsItem?>?,
    var categoryName: String?
) : RecyclerView.Adapter<AllHorizontalListAdapter.ViewHolder>() {
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): AllHorizontalListAdapter.ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val view: View = layoutInflater.inflate(R.layout.all_horizontal_list, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: AllHorizontalListAdapter.ViewHolder, position: Int) {
        if(itemDetailsArrayList!!.get(position)!!.image1 != null) {
            Glide.with(activity)
                .load(itemDetailsArrayList!!.get(position)!!.image1)
                .placeholder(R.drawable.ic_placeholder_image)
                .error(R.drawable.ic_placeholder_image)
                .into(holder.imageTV)
        }

        if(itemDetailsArrayList!!.get(position)!!.isFav.equals("1")){
            holder.favTV.setBackgroundResource(R.drawable.ic_fav)
        }
        else if(itemDetailsArrayList!!.get(position)!!.isFav.equals("2")){
            holder.favTV.setBackgroundResource(R.drawable.ic_unfav)
        }


        holder.itemView.setOnClickListener {
            val intent = Intent(activity, DetailActivity::class.java)
            intent.putExtra("category_name",categoryName)
            intent.putExtra("item_id",itemDetailsArrayList!!.get(position)!!.itemId)
//            intent.putExtra("category_id",itemDetailsArrayList!!.get(position)!!.categoryId)
//            intent.putExtra("description",itemDetailsArrayList!!.get(position)!!.description)
//            intent.putExtra("image1",itemDetailsArrayList!!.get(position)!!.image1)
//            intent.putExtra("image2",itemDetailsArrayList!!.get(position)!!.image2)
//            intent.putExtra("image3",itemDetailsArrayList!!.get(position)!!.image3)
//            intent.putExtra("image4",itemDetailsArrayList!!.get(position)!!.image4)
//            intent.putExtra("image5",itemDetailsArrayList!!.get(position)!!.imag5)
//            intent.putExtra("is_fav",itemDetailsArrayList!!.get(position)!!.isFav)
            activity.startActivity(intent)
        }
    }

    override fun getItemCount(): Int {
     return itemDetailsArrayList!!.size
    }
    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
           var imageTV:ImageView
           var favTV:TextView
        init {
            imageTV=itemView.findViewById(R.id.imageTV)
            favTV=itemView.findViewById(R.id.favTV)
        }
    }
}
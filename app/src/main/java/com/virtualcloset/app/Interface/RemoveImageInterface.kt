package com.virtualcloset.app.Interface

interface RemoveImageInterface {
    fun onRemoveClick(pos: Int, type: String?)
}
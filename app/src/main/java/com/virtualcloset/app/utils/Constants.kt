package com.virtualcloset.app.utils

// - - Constant Values
const val SPLASH_TIME_OUT = 1500

//staging Base url
//const val BASE_URL ="https://thevcloset.com/staging/WebServices/"

//live url
const val BASE_URL ="https://thevcloset.com/WebServices/"

const val TERMS_AND_CONDITIONS =BASE_URL+"TermsandConditions.html"
const val ABOUT =BASE_URL+"Aboutus.html"
const val PRIVACY_POLICY= BASE_URL+"PrivacyPolicy.html"


const val ALL_TAG = "all_tag"
const val FAVOURITE_TAG="favourite_tag"


const val NOTI_TYPE = "noti_type"
/*
* Shared Preferences Keys
* */
const val IS_LOGIN = "is_login"
const val USERID = "user_id"
const val NAME = "name"
const val EMAIL = "email"
const val PASSWORD ="password"
const val VERIFICATIONCODE = "verification_code"
const val EMAILVERIFICATION = "email_verification"
const val DEVICENAME ="device_name"
const val DISABLE = "disable"
const val IMAGE = "image"
const val AUTHTOKEN = "authtoken"


const val LICENCE_KEY="MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAiM9mscXA0Rp52P98sMq2DqKz3sokpFjRK9fT/GxAlieGMhdEWsl4mq731O9DwU2fXDnsdtrmbY7gq36a7Og9SII4v6C2wAg99rC1kGpminje7JAfprDWP7prkomVkiaONb9OhUR/nDtj9/GSWJdNaL0nQmge9XYl2kmXI/yMq59NfLCsp9Eeaoiq9aUVSnyZz9wVPT02qirsuzW8WWRmOVAe3QcvynNFp9bLCCS5LrW+MhETiqF33nWfahdDtNxYGiQYvQCs/mtHQcXgmfASBC9TAQDbmY6UeKb2LeslmNiWtU+pbPgOI0ZGNG7evFwxmS5I+NerCvP4n7DD7taImwIDAQAB"
const val MERCHANT_ID="230260964835016495"
const val PRODUCT_ID = "one_time_in_app_product_123"


const val PREF_FILE = "MyPref"
const val PURCHASE_KEY = "purchase item"


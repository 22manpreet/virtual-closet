package com.virtualcloset.app.utils

import android.content.Context
import android.content.SharedPreferences

class AppPrefrences {
    /***************
     * Define SharedPrefrances Keys & MODE
     */
    val PREF_NAME = "App_PREF"
    val MODE = Context.MODE_PRIVATE

    /*
         * Write the Boolean Value
         * */
    public fun writeBoolean(
        context: Context,
        key: String?,
        value: Boolean
    ) {
        getEditor(context).putBoolean(key, value).apply()
    }
    /*
       * Read the Boolean Valueget
       * */
    public fun readBoolean(
        context: Context,
        key: String?,
        defValue: Boolean
    ): Boolean {
        return getPreferences(context).getBoolean(key, defValue)
    }
    /*
         * Write the String Value
         * */
    public fun writeString(
        context: Context,
        key: String?,
        value: String?
    ) {
        getEditor(context).putString(key, value).apply()
    }
    /*
     * Read the String Value
     * */
    public  fun readString(
        context: Context,
        key: String?,
        defValue: String?
    ): String? {
        return getPreferences(context).getString(key, defValue)
    }

    /*
     * Return the SharedPreferences
     * with
     * @Prefreces Name & Prefreces = MODE
     * */
    public fun getPreferences(context: Context): SharedPreferences {
        return context.getSharedPreferences(PREF_NAME, MODE)
    }

    /*
     * Return the SharedPreferences Editor
     * */
    public fun getEditor(context: Context): SharedPreferences.Editor {
        return getPreferences(context).edit()
    }
}
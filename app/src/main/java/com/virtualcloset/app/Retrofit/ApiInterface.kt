package com.virtualcloset.app.Retrofit

import com.virtualcloset.app.Model.*
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.http.*

interface ApiInterface {

    @Multipart
    @POST("SignUp.php")
    fun SignUpDataRequest(
        @Part("name") name: RequestBody?,
        @Part("email") email: RequestBody?,
        @Part("password") password: RequestBody?,
        @Part("device_name") device_name: RequestBody?,
        @Part("disable")disable :RequestBody?
    ): Call<SignUpModel>

    @Multipart
    @POST("Login.php")
    fun LogInDataRequest(
        @Part("email") email: RequestBody?,
        @Part("password") password: RequestBody?
    ): Call<LoginModel>


    @Multipart
    @POST("ForgotPassword.php")
    fun ForgotPasswordDataRequest(
        @Part("email") email: RequestBody?
    ): Call<ForgotPasswordModel>

    @POST("LogOut.php")
    fun LogOutDataRequest(
        @HeaderMap Token: Map<String, String>?
    ): Call<LogOutModel>

    @Multipart
    @POST("ChangePassword.php")
    fun ChangePasswordDataRequest(
        @HeaderMap Token: Map<String, String>?,
        @Part("old_password") current_password: RequestBody?,
        @Part("new_password") new_password: RequestBody?
    ): Call<ChangePasswordModel>

    @POST("GetProfile.php")
    fun GetProfileDataRequest(
        @HeaderMap Token: Map<String, String>?
    ): Call<GetProfileModel>


    @Multipart
    @POST("EditProfile.php")
    fun EditProfileDataRequest(
        @HeaderMap Token: Map<String, String>?,
        @Part("name") name: RequestBody?,
        @Part("email") email: RequestBody?,
        @Part image: MultipartBody.Part?
    ): Call<EditProfileModel>


    @Multipart
    @POST("GetAllItemsByCategory.php")
    fun GetAllItemsByCategoryDataRequest(
        @HeaderMap Token: Map<String, String>?,
        @Part("is_status") is_status: RequestBody?,
    ): Call<GetAllItemsByCategoryModel>

    @Multipart
    @POST("DeleteItem.php")
    fun DeleteItemDataRequest(
        @HeaderMap Token: Map<String, String>?,
        @Part("item_id") item_id: RequestBody?,
    ): Call<DeleteItemModel>


    @POST("GetAllCategory.php")
    fun GetAllCategoryDataRequest(
        @HeaderMap Token: Map<String, String>?
    ): Call<GetAllCategoryModel>

    @Multipart
    @POST("AddCategory.php")
    fun AddCategoryDataRequest(
        @HeaderMap Token: Map<String, String>?,
        @Part("category_name") category_name: RequestBody?,
        @Part("role") role: RequestBody?,
        @Part("is_remove") is_remove: RequestBody?,
    ): Call<AddCategoryModel>


    @Multipart
    @POST("AddItems.php")
    fun AddItemsDataRequest(
        @HeaderMap Token: Map<String, String>?,
        @Part("category_id") category_id: RequestBody?,
        @Part("description") description: RequestBody?,
        @Part image1: MultipartBody.Part?,
        @Part image2: MultipartBody.Part?,
        @Part image3: MultipartBody.Part?,
        @Part image4: MultipartBody.Part?,
        @Part image5: MultipartBody.Part?
    ): Call<AddItemsModel>


    @Multipart
    @POST("AddSearch.php")
    fun AddSearchDataRequest(
        @HeaderMap Token: Map<String, String>?,
        @Part("search") search: RequestBody?
    ): Call<AddSearchModel>


    @Multipart
    @POST("GetAllItemBySearch.php")
    fun GetAllItemBySearchDataRequest(
        @HeaderMap Token: Map<String, String>?,
        @Part("pageNo") pageNo: RequestBody?,
        @Part("per_page") per_page: RequestBody?,
        @Part("search") search: RequestBody?
    ): Call<GetAllItemBySearchModel>


    @Multipart
    @POST("GetItemsDetailsById.php")
    fun GetItemsDetailsByIdDataRequest(
        @HeaderMap Token: Map<String, String>?,
        @Part("item_id") item_id: RequestBody?
    ): Call<GetItemDetailedByIdModel>


    @Multipart
    @POST("AddEditFavItems.php")
    fun addEditFavItemsDataRequest(
        @HeaderMap Token: Map<String, String>?,
        @Part("item_id") item_id: RequestBody?,
        @Part("favourite") favourite : RequestBody?
    ): Call<AddEditFavItemsModel>


    @Multipart
    @POST("GetAllItemDetailByCategoryId.php")
    fun GetAllItemDetailByCategoryIdDataRequest(
        @HeaderMap Token: Map<String, String>?,
        @Part("category_id") category_id: RequestBody?,
        @Part("pageNo") pageNo: RequestBody?,
        @Part("per_page") per_page: RequestBody?
    ): Call<GetAllItemDetailByCategoryIdModel>

    @Multipart
    @POST("Getallitemfavdetailsbycategory.php")
    fun GetallitemfavdetailsbycategoryDataRequest(
        @HeaderMap Token: Map<String, String>?,
        @Part("category_id") category_id: RequestBody?,
        @Part("pageNo") pageNo: RequestBody?,
        @Part("per_page") per_page: RequestBody?
    ): Call<GetallitemfavdetailsbycategoryModel>

    @POST("SubscriptionStatus.php")
    fun SubscriptionStatusDataRequest(
        @HeaderMap Token: Map<String, String>?
    ): Call<SubscriptionStatusModel>


    @POST("AddSubscription.php")
    fun AddSubscriptionDataRequest(
        @HeaderMap Token: Map<String, String>?,
        @Body mParams: MutableMap<String?, Any?>
    ): Call<AddSubscriptionModel>

}
package com.virtualcloset.app.fragment

import android.annotation.SuppressLint
import android.content.Intent
import android.opengl.Visibility
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import butterknife.Unbinder
import com.virtualcloset.app.Model.DataItem
import com.virtualcloset.app.Model.GetAllItemsByCategoryModel
import com.virtualcloset.app.R
import com.virtualcloset.app.Retrofit.RetrofitClient
import com.virtualcloset.app.activity.AddToClosetActivity
import com.virtualcloset.app.adapter.AllFavVerticalListAdapter
import com.virtualcloset.app.adapter.AllVerticalListAdapter
import kotlinx.android.synthetic.main.fragment_all.*
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.HashMap

class FavoriteFragment : BaseFragment() {

    // - - Initialize Widgets
    lateinit var mUnbinder: Unbinder

    @BindView(R.id.favRV)
    lateinit var favRV: RecyclerView

    @BindView(R.id.addIV)
    lateinit var addIV: ImageView

    @BindView(R.id.messageTV)
    lateinit var messageTV: TextView

    var allFavVerticalListAdapter: AllFavVerticalListAdapter? = null
    var allItemsArrayList: ArrayList<DataItem?>? = ArrayList()
    @SuppressLint("UseRequireInsteadOfGet")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment

        val view: View = inflater.inflate(R.layout.fragment_favorite, container, false)
        mUnbinder = ButterKnife.bind(this, view)

        return view
    }

    @SuppressLint("UseRequireInsteadOfGet")
    override fun onResume() {
        super.onResume()
        if (isNetworkAvailable(activity!!)) {
            executeGetAllItemsByCategoryRequest()
        } else {
            showToast(activity, getString(R.string.internet_connection_error))
        }
    }
    private fun executeGetAllItemsByCategoryRequest() {
        val headers: MutableMap<String, String> = HashMap()
        headers["Token"] = getAuthToken()

        val is_status: RequestBody = "2"
            .toRequestBody("multipart/form-data".toMediaTypeOrNull())

        showProgressDialog(activity)
        val call = RetrofitClient.apiInterface.GetAllItemsByCategoryDataRequest(
            headers,
            is_status
        )

        call.enqueue(object : Callback<GetAllItemsByCategoryModel> {
            override fun onFailure(call: Call<GetAllItemsByCategoryModel>, t: Throwable) {
                Log.e(TAG, t.message.toString())
                dismissProgressDialog()
            }
            override fun onResponse(call: Call<GetAllItemsByCategoryModel>, response: Response<GetAllItemsByCategoryModel>) {
                Log.e(TAG, response.body().toString())
                dismissProgressDialog()
                var  mGetAllItemsByCategoryModel = response.body()
                if (mGetAllItemsByCategoryModel!!.status == 1) {
                    messageTV.visibility=View.GONE
                    favRV.visibility=View.VISIBLE
                    allItemsArrayList=mGetAllItemsByCategoryModel.data
                    setAdapter(allItemsArrayList)

                }
                else if(mGetAllItemsByCategoryModel!!.status == 0){
                    messageTV.visibility=View.VISIBLE
                    favRV.visibility=View.GONE
                }
                else {
                    showAlertDialog(activity, getString(R.string.internal_server_error))
                }
            }
        })
    }


    @OnClick(
        R.id.addIV
    )
    fun onViewClicked(view: View){
        when(view.id) {
            R.id.addIV -> performAddBtnClick()
        }
    }
    private fun performAddBtnClick() {
        val i = Intent(activity, AddToClosetActivity::class.java)
        startActivity(i)
    }
    @SuppressLint("UseRequireInsteadOfGet")
    private fun setAdapter(allItemsArrayList: ArrayList<DataItem?>?) {
        val layoutManager: RecyclerView.LayoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        favRV.setLayoutManager(layoutManager)
        if(activity!=null) {
            allFavVerticalListAdapter = AllFavVerticalListAdapter(activity!!, allItemsArrayList)
            favRV.setAdapter(allFavVerticalListAdapter)
        }
    }

}
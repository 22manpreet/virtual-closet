package com.virtualcloset.app.fragment

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.text.format.DateFormat
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.core.app.ActivityCompat.recreate
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import butterknife.Unbinder
import com.android.billingclient.api.*
import com.anjlab.android.iab.v3.BillingProcessor
import com.anjlab.android.iab.v3.TransactionDetails
import com.virtualcloset.app.Model.AddSubscriptionModel
import com.virtualcloset.app.Model.DataItem
import com.virtualcloset.app.Model.GetAllItemsByCategoryModel
import com.virtualcloset.app.Model.SubscriptionDetailModel
import com.virtualcloset.app.R
import com.virtualcloset.app.Retrofit.RetrofitClient
import com.virtualcloset.app.activity.AddToClosetActivity
import com.virtualcloset.app.activity.HomeActivity
import com.virtualcloset.app.activity.Security
import com.virtualcloset.app.adapter.AllVerticalListAdapter
import com.virtualcloset.app.utils.LICENCE_KEY
import com.virtualcloset.app.utils.MERCHANT_ID
import com.virtualcloset.app.utils.PRODUCT_ID
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class AllFragment : BaseFragment(), BillingProcessor.IBillingHandler //, PurchasesUpdatedListener
{
    // - - Initialize Widgets
    lateinit var mUnbinder: Unbinder

    @BindView(R.id.allRV)
    lateinit var allRV: RecyclerView

    @BindView(R.id.addIV)
    lateinit var addIV: ImageView

    @BindView(R.id.messageTV)
    lateinit var messageTV: TextView

    var allVerticalListAdapter: AllVerticalListAdapter? = null
    @SuppressLint("UseRequireInsteadOfGet")

    var allItemsArrayList: ArrayList<DataItem?>? = ArrayList()

    private var billingClient: BillingClient? = null
    var purchaseTime: String? = null
    var subscriptionDetail: ArrayList<SubscriptionDetailModel?>? = ArrayList()

    var subscription_price:String?=null
    // var subscriptionalertDialog=Dialog(requireActivity())
    var subscriptionalertDialog: Dialog? = null

    var mBillingProcessor: BillingProcessor? = null

    @SuppressLint("UseRequireInsteadOfGet")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view: View = inflater.inflate(R.layout.fragment_all, container, false)
        mUnbinder = ButterKnife.bind(this, view)
      //  subscriptionalertDialog = Dialog(activity!!)
        initializeSubscription()

//        billingClient = BillingClient.newBuilder(activity!!).enablePendingPurchases().setListener(this).build()
//        billingClient!!.startConnection(object : BillingClientStateListener {
//            override fun onBillingSetupFinished(billingResult: BillingResult) {
//                if (billingResult.responseCode == BillingClient.BillingResponseCode.OK) {
//                    val queryPurchase = billingClient!!.queryPurchases(BillingClient.SkuType.INAPP)
//                    val queryPurchases: List<Purchase>? = queryPurchase.purchasesList
//                    for(i in 0..queryPurchases!!.size) {
//                        purchaseTime= queryPurchases!!.get(i).purchaseTime.toString()
//                        //  queryPurchases!!.get(i).purchaseTime.also { purchaseTime = it }
//                    }
//
//                    if (queryPurchases != null && queryPurchases.size > 0) {
//                        handlePurchases(queryPurchases)
//                    }
//                    //if purchase list is empty that means item is not purchased
//                    //Or purchase is refunded or canceled
//                    else{
//                        savePurchaseValueToPref(false)
//                    }
//                }
//            }
//
//            override fun onBillingServiceDisconnected() {}
//        })

//        //item Purchased
//        if (purchaseValueFromPref) {
//         //   purchase_button!!.visibility = View.GONE
//            showToast(activity,"Purchase Status : Purchased")
//
//            executeSubscriptionDataRequest(purchaseTime)
//
////            val i = Intent(mActivity, HomeActivity::class.java)
////            startActivity(i)
//        }
//        //item not Purchased
//        else {
//          //  purchase_button!!.visibility = View.VISIBLE
//            showToast(activity,"Purchase Status : Not Purchased")
//        }

        return view
    }

    @SuppressLint("UseRequireInsteadOfGet")
    override fun onResume() {
        super.onResume()
        addIV.setClickable(true)
        if (isNetworkAvailable(activity!!)) {
            executeGetAllItemsByCategoryRequest()
        } else {
            showToast(activity, getString(R.string.internet_connection_error))
        }
    }
    private fun executeGetAllItemsByCategoryRequest() {
        val headers: MutableMap<String, String> = HashMap()
        headers["Token"] = getAuthToken()

        val is_status: RequestBody = "1".toRequestBody("multipart/form-data".toMediaTypeOrNull())

    //    showProgressDialog(activity)
        val call = RetrofitClient.apiInterface.GetAllItemsByCategoryDataRequest(
            headers,
            is_status
        )

        call.enqueue(object : Callback<GetAllItemsByCategoryModel> {
            override fun onFailure(call: Call<GetAllItemsByCategoryModel>, t: Throwable) {
                Log.e(TAG, t.message.toString())
     //           dismissProgressDialog()
            }
            override fun onResponse(call: Call<GetAllItemsByCategoryModel>, response: Response<GetAllItemsByCategoryModel>) {
                Log.e(TAG, response.body().toString())
     //           dismissProgressDialog()

                var  mGetAllItemsByCategoryModel = response.body()
             //   showToast(activity,"getAllItemStatus : "+mGetAllItemsByCategoryModel!!.status)
                if (mGetAllItemsByCategoryModel!!.status == 1) {
                    messageTV.visibility=View.GONE
                    allRV.visibility=View.VISIBLE
                    allItemsArrayList=mGetAllItemsByCategoryModel.data
                    setAdapter(allItemsArrayList)
                }
                else if(mGetAllItemsByCategoryModel!!.status == 0){
                    messageTV.visibility=View.VISIBLE
                    allRV.visibility=View.GONE
                }
                else if(mGetAllItemsByCategoryModel!!.status == 7){
                    showAlertPurchaseDialog(getString(R.string.your_free_trial_plan_is_over_now_you_need_to_pay_to_continue_using_app))
                }
                else {
                    showToast(activity, mGetAllItemsByCategoryModel!!.status.toString())
                   // showAlertDialog(activity, getString(R.string.internal_server_error))
                    showAlertDialog(activity,mGetAllItemsByCategoryModel!!.status.toString())
                }
            }
        })
    }

    @SuppressLint("UseRequireInsteadOfGet")
    private fun showAlertPurchaseDialog(message: String?) {
        subscriptionalertDialog = Dialog(activity!!)
        subscriptionalertDialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        subscriptionalertDialog!!.setContentView(R.layout.dialog_alert_subscription)
        subscriptionalertDialog!!.setCanceledOnTouchOutside(false)
        subscriptionalertDialog!!.setCancelable(false)
        subscriptionalertDialog!!.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        // set the custom dialog components - text, image and button
        val txtMessageTV = subscriptionalertDialog!!.findViewById<TextView>(R.id.txtMessageTV)
        val btnSubscription = subscriptionalertDialog!!.findViewById<TextView>(R.id.btnSubscription)
        txtMessageTV.text = message
        btnSubscription.setOnClickListener {
      //      performPurchaseClick()
            subscriptionalertDialog!!.dismiss()
            performSubscriptionClick()

        }
        subscriptionalertDialog!!.show()
    }

    private fun performSubscriptionClick() {
        subscription_price="765.10"
        subscriptionCallBack(PRODUCT_ID)
    }
    @SuppressLint("UseRequireInsteadOfGet")
    open fun initializeSubscription() {
        mBillingProcessor = BillingProcessor(activity!!, LICENCE_KEY, this)
        mBillingProcessor!!.initialize()
    }
    @SuppressLint("UseRequireInsteadOfGet")
    private fun subscriptionCallBack(SUBSCRIPTION_PLAN_KEY: String) {
        // With Testing  PayLoads for Product Purchase & Subscription
        //mBillingProcessor.purchase(getActivity(), "android.test.purchased", null /*or developer payload*/, extraParams);
        //mBillingProcessor.subscribe(getActivity(), "android.test.purchased", null /*or developer payload*/, extraParams);
        if (isNetworkAvailable(activity!!)) {
            val extraParams = Bundle()
            extraParams.putString("accountId", MERCHANT_ID)
            mBillingProcessor!!.purchase(activity!!, SUBSCRIPTION_PLAN_KEY, null , extraParams)
        } else {
            showToast(activity!!, getString(R.string.internet_connection_error))
        }
    }
    @SuppressLint("UseRequireInsteadOfGet")
    override fun onProductPurchased(productId: String, mTransactionDetails: TransactionDetails?) {
        Log.e(TAG, "Purchase DATA :- " + mTransactionDetails!!.purchaseInfo.purchaseData)
        /*
         * Called when requested PRODUCT ID was successfully purchased
         */
        val mPurchaseData = mTransactionDetails.purchaseInfo.purchaseData
        var mSubscriptionPlanKey = mTransactionDetails.purchaseInfo.purchaseData.productId

        Log.e(TAG, "onProduct Purchased_Data: " + mTransactionDetails.purchaseInfo.purchaseData.toString())
        Log.e(TAG, "onProduct Purchased_OrderID: " + mPurchaseData!!.orderId)
        Log.e(TAG, "onProduct Purchased_Token: " + mPurchaseData.purchaseToken)
        Log.e(TAG, "onProduct Purchased_Time: " + mPurchaseData.purchaseTime)
        Log.e(TAG, "Subscriptions Plan Key: $mSubscriptionPlanKey")
        showToast(activity,"purchase success")
//        if (mTransactionDetails.purchaseInfo == null) {
//            AppPrefrences().writeBoolean(activity!!, IS_SUBSCRIPTION, false)
//            showToast(activity,getString(R.string.some_things))
//        } else {
//            AppPrefrences().writeBoolean(activity!!, IS_SUBSCRIPTION, true)
//            showToast(activity,getString(R.string.purchase_subscription_Successfully))
//            if(isNetworkAvailable(activity!!)){
//                executeSubscriptionDataRequest(mPurchaseData.purchaseTime)
//            } else {
//                showToast(activity, getString(R.string.internet_connection_error))
//            }
//        }
//        activity!!.finish()

        if(isNetworkAvailable(activity!!)){
            executeSubscriptionDataRequest(mPurchaseData.purchaseTime)
        } else {
            showToast(activity, getString(R.string.internet_connection_error))
        }
    }
    override fun onPurchaseHistoryRestored() {

    }

    override fun onBillingError(errorCode: Int, error: Throwable?) {
        /*
        * Called when some error occurred. See Constants class for more details
        *
        * Note - this includes handling the case where the user canceled the buy dialog:
        * errorCode = Constants.BILLING_RESPONSE_RESULT_USER_CANCELED
        */
        Log.e(TAG, "onBillingError: ")
        when (errorCode) {
            0 -> Log.e("onBillingError", "> Success - BILLING_RESPONSE_RESULT_OK")
            1 -> Log.e(
                "onBillingError",
                "> User pressed back or canceled a dialog - BILLING_RESPONSE_RESULT_USER_CANCELED"
            )
            3 -> Log.e(
                "onBillingError",
                "> Billing API version is not supported for the type requested - BILLING_RESPONSE_RESULT_BILLING_UNAVAILABLE"
            )
            4 -> Log.e(
                "onBillingError",
                "> Requested product is not available for purchase - BILLING_RESPONSE_RESULT_ITEM_UNAVAILABLE"
            )
            5 -> Log.e(
                "onBillingError",
                "> Invalid arguments provided to the API. This error can also indicate that the application was not correctly signed or properly set up for In-app Billing in Google Play, or does not have the necessary permissions in its manifest - BILLING_RESPONSE_RESULT_DEVELOPER_ERROR"
            )
            6 -> Log.e(
                "onBillingError",
                "> Fatal error during the API action - BILLING_RESPONSE_RESULT_ERROR"
            )
            7 -> Log.e(
                "onBillingError",
                "> Failure to purchase since item is already owned - BILLING_RESPONSE_RESULT_ITEM_ALREADY_OWNED"
            )
            8 -> Log.e(
                "onBillingError",
                "> Failure to consume since item is not owned - BILLING_RESPONSE_RESULT_ITEM_NOT_OWNED"
            )
        }
    }

    override fun onBillingInitialized() {
        /*
        * Called when BillingProcessor was initialized and it's ready to purchase
        */
        Log.e(TAG, "onBillingInitialized: ");
    }

    override fun onDestroy() {
        if (mBillingProcessor != null) mBillingProcessor!!.release()
        super.onDestroy()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (!mBillingProcessor!!.handleActivityResult(requestCode, resultCode, data)) super.onActivityResult(
            requestCode,
            resultCode,
            data
        )
    }
    //    @SuppressLint("UseRequireInsteadOfGet")
//    private fun performPurchaseClick() {
//        if (billingClient!!.isReady) {
//            initiatePurchase()
//        }
//        //else reconnect service
//        else {
//            billingClient = BillingClient.newBuilder(activity!!).enablePendingPurchases().setListener(this).build()
//            billingClient!!.startConnection(object : BillingClientStateListener {
//                override fun onBillingSetupFinished(billingResult: BillingResult) {
//                    if (billingResult.responseCode == BillingClient.BillingResponseCode.OK) {
//                        initiatePurchase()
//                    } else {
//                        Toast.makeText(activity, "Error " + billingResult.debugMessage, Toast.LENGTH_SHORT).show()
//                    }
//                }
//
//                override fun onBillingServiceDisconnected() {}
//            })
//        }
//    }
    private fun mParam(subscriptionDetail: ArrayList<SubscriptionDetailModel?>): MutableMap<String?, Any?> {
        val mMap: MutableMap<String?, Any?> = HashMap()
        mMap["plan_id"] = "1"
        mMap["subscription_details"]= subscriptionDetail
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }
    private fun executeSubscriptionDataRequest(purchaseTime: Date?) {
//        var date: Date? = purchaseTime
//        val dateString: String = DateFormat.format("yyyy-MM-dd HH:mm:ss",date).toString()
      //  val dateString: String = DateFormat.format("yyyy-MM-dd HH:mm:ss zzzz yyyy",date).toString()
//        var timeD: Date? = purchaseTime
//        var sdf = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
//      //  sdf.timeZone= TimeZone.getTimeZone("Etc/GMT")
//    //    sdf.setTimeZone(TimeZone.getTimeZone("Etc/GMT"))
//        val dateString = sdf.format(timeD)


        val headers: MutableMap<String, String> = HashMap()
        headers["Token"] = getAuthToken()

        subscriptionDetail!!.add(SubscriptionDetailModel("","","", PRODUCT_ID))

     //   showProgressDialog(activity)
        val call = RetrofitClient.apiInterface.AddSubscriptionDataRequest(headers,mParam(subscriptionDetail!!))

        call.enqueue(object : Callback<AddSubscriptionModel> {
            override fun onFailure(call: Call<AddSubscriptionModel>, t: Throwable) {
                Log.e(TAG, t.message.toString())
       //         showToast(activity,"onFailure")
     //           dismissProgressDialog()
            }
            override fun onResponse(call: Call<AddSubscriptionModel>, response: Response<AddSubscriptionModel>) {
                Log.e(TAG, response.body().toString())
    //            dismissProgressDialog()
                var  mAddSubscriptionModel = response.body()
            //    showToast(activity,"subscription status: "+mAddSubscriptionModel!!.status.toString())
                if (mAddSubscriptionModel!!.status == 1) {
                  //  showToast(activity,mAddSubscriptionModel.message)
                    subscriptionalertDialog!!.dismiss()
                    executeGetAllItemsByCategoryRequest()
                }
                else if(mAddSubscriptionModel!!.status == 0){
                    showToast(activity,mAddSubscriptionModel.message)
                }
                else {
                      showToast(activity,mAddSubscriptionModel.message)
                      showAlertDialog(activity,mAddSubscriptionModel.message)
                }
            }
        })
    }
//    private val preferenceObject: SharedPreferences
//        @SuppressLint("UseRequireInsteadOfGet")
//        get() = activity!!.getSharedPreferences(PREF_FILE, 0)
//
//    private val preferenceEditObject: SharedPreferences.Editor
//        @SuppressLint("UseRequireInsteadOfGet")
//        get() {
//            val pref: SharedPreferences = activity!!.getSharedPreferences(PREF_FILE, 0)
//            return pref.edit()
//        }
//
//    private val purchaseValueFromPref: Boolean
//        get() = preferenceObject.getBoolean(PURCHASE_KEY, false)
//
//    private fun savePurchaseValueToPref(value: Boolean) {
//        preferenceEditObject.putBoolean(PURCHASE_KEY, value).commit()
//    }
//
//    @SuppressLint("UseRequireInsteadOfGet")
//    private fun initiatePurchase() {
//
//        val skuList: MutableList<String> = ArrayList()
//        skuList.add(PRODUCT_ID)
//        val params = SkuDetailsParams.newBuilder()
//        params.setSkusList(skuList).setType(BillingClient.SkuType.INAPP)
//
//        billingClient!!.querySkuDetailsAsync(params.build()) { billingResult, skuDetailsList ->
//            if (billingResult.responseCode == BillingClient.BillingResponseCode.OK) {
//                if (skuDetailsList != null && skuDetailsList.size > 0) {
//                    val flowParams = BillingFlowParams.newBuilder()
//                        .setSkuDetails(skuDetailsList[0])
//                        .build()
//                    billingClient!!.launchBillingFlow(activity!!, flowParams)
//                } else {
//                    //try to add item/product id "purchase" inside managed product in google play console
//
//                    Toast.makeText(activity, "Purchase Item not Found", Toast.LENGTH_SHORT).show()
//                }
//            } else {
//                Toast.makeText(activity,
//                    " Error " + billingResult.debugMessage, Toast.LENGTH_SHORT).show()
//            }
//        }
//    }
//
//    override fun onPurchasesUpdated(billingResult: BillingResult, purchases: List<Purchase>?) {
//        //if item newly purchased
//
//        if (billingResult.responseCode == BillingClient.BillingResponseCode.OK && purchases != null) {
//            handlePurchases(purchases)
//        }
//        //if item already purchased then check and reflect changes
//        else if (billingResult.responseCode == BillingClient.BillingResponseCode.ITEM_ALREADY_OWNED) {
//            val queryAlreadyPurchasesResult = billingClient!!.queryPurchases(BillingClient.SkuType.INAPP)
//            val alreadyPurchases: List<Purchase>? = queryAlreadyPurchasesResult.purchasesList
//            if (alreadyPurchases != null) {
//                handlePurchases(alreadyPurchases)
//            }
//        }
//        //if purchase cancelled
//        else if (billingResult.responseCode == BillingClient.BillingResponseCode.USER_CANCELED) {
//            Toast.makeText(activity, "Purchase Canceled", Toast.LENGTH_SHORT).show()
//        }
//        // Handle any other error msgs
//        else {
//            Toast.makeText(activity, "Error " + billingResult.debugMessage, Toast.LENGTH_SHORT).show()
//        }
//    }
//
//    @SuppressLint("UseRequireInsteadOfGet")
//    fun handlePurchases(purchases: List<Purchase>) {
//        for (purchase in purchases) {
//            //if item is purchased
//
//            if (PRODUCT_ID == purchase.skus.toString() && purchase.purchaseState == Purchase.PurchaseState.PURCHASED) {
//                if (!verifyValidSignature(purchase.originalJson, purchase.signature)) {
//                    // Invalid purchase
//                    // show error to user
//
//                    Toast.makeText(activity, "Error : Invalid Purchase", Toast.LENGTH_SHORT).show()
//                    return
//                }
//                // else purchase is valid
//                //if item is purchased and not acknowledged
//
//
//                if (!purchase.isAcknowledged) {
//                    val acknowledgePurchaseParams = AcknowledgePurchaseParams.newBuilder()
//                        .setPurchaseToken(purchase.purchaseToken)
//                        .build()
//                    billingClient!!.acknowledgePurchase(acknowledgePurchaseParams, ackPurchase)
//                    showToast(activity,"purchase1" +"NotAcknowledge")
//                }
//                //else item is purchased and also acknowledged
//                else {
//                    // Grant entitlement to the user on item purchase
//                    // restart activity
//
//                   // if (!purchaseValueFromPref) {
//                        savePurchaseValueToPref(true)
//                        Toast.makeText(activity, "Item Purchased", Toast.LENGTH_SHORT).show()
//                        recreate(activity!!)
//                        purchaseTime=purchase.purchaseTime.toString()
//                        showToast(activity,"purchase1" +purchaseTime)
//                        executeSubscriptionDataRequest(purchaseTime)
////                        val i = Intent(mActivity, HomeActivity::class.java)
////                        startActivity(i)
//                   // }
//                }
//            }
//            //if purchase is pending
//            else if (PRODUCT_ID == purchase.skus.toString() && purchase.purchaseState == Purchase.PurchaseState.PENDING) {
//                Toast.makeText(activity,
//                    "Purchase is Pending. Please complete Transaction", Toast.LENGTH_SHORT).show()
//            }
//            //if purchase is refunded or unknown
//            else if (PRODUCT_ID == purchase.skus.toString() && purchase.purchaseState == Purchase.PurchaseState.UNSPECIFIED_STATE) {
//                savePurchaseValueToPref(false)
//                showToast(activity,"Purchase Status : Not Purchased")
//           //     purchase_button!!.visibility = View.VISIBLE
//                Toast.makeText(activity, "Purchase Status Unknown", Toast.LENGTH_SHORT).show()
//            }
//        }
//    }
//
//    @SuppressLint("UseRequireInsteadOfGet")
//    var ackPurchase = AcknowledgePurchaseResponseListener { billingResult ->
//        if (billingResult.responseCode == BillingClient.BillingResponseCode.OK) {
//            //if purchase is acknowledged
//            // Grant entitlement to the user. and restart activity
//
//            savePurchaseValueToPref(true)
//            Toast.makeText(activity, "Item Purchased", Toast.LENGTH_SHORT).show()
//            recreate(activity!!)
//            showToast(activity,"purchase2 "+purchaseTime)
//            executeSubscriptionDataRequest(purchaseTime)
////            val i = Intent(mActivity, HomeActivity::class.java)
////            startActivity(i)
//        }
//    }
//    /**
//     * Verifies that the purchase was signed correctly for this developer's public key.
//     *
//     * Note: It's strongly recommended to perform such check on your backend since hackers can
//     * replace this method with "constant true" if they decompile/rebuild your app.
//     *
//     */
//    private fun verifyValidSignature(signedData: String, signature: String): Boolean {
//        return try {
//            // To get key go to Developer Console > Select your app > Development Tools > Services & APIs.
//
//            val base64Key = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAiM9mscXA0Rp52P98sMq2DqKz3sokpFjRK9fT/GxAlieGMhdEWsl4mq731O9DwU2fXDnsdtrmbY7gq36a7Og9SII4v6C2wAg99rC1kGpminje7JAfprDWP7prkomVkiaONb9OhUR/nDtj9/GSWJdNaL0nQmge9XYl2kmXI/yMq59NfLCsp9Eeaoiq9aUVSnyZz9wVPT02qirsuzW8WWRmOVAe3QcvynNFp9bLCCS5LrW+MhETiqF33nWfahdDtNxYGiQYvQCs/mtHQcXgmfASBC9TAQDbmY6UeKb2LeslmNiWtU+pbPgOI0ZGNG7evFwxmS5I+NerCvP4n7DD7taImwIDAQAB"
//            Security.verifyPurchase(base64Key, signedData, signature)
//        } catch (e: IOException) {
//            false
//        }
//    }
//
//    override fun onDestroy() {
//        super.onDestroy()
//        if (billingClient != null) {
//            billingClient!!.endConnection()
//        }
//    }
//
//    companion object {
//        const val PREF_FILE = "MyPref"
//        const val PURCHASE_KEY = "purchase item"
//        const val PRODUCT_ID = "one_time_in_app_product_123"
//    }
    @OnClick(
        R.id.addIV
    )
    fun onViewClicked(view: View){
        when(view.id) {
            R.id.addIV -> performAddBtnClick()
        }
    }

    private fun performAddBtnClick() {
        addIV.setClickable(false)
        val i = Intent(activity, AddToClosetActivity::class.java)
        startActivity(i)
    }

    @SuppressLint("UseRequireInsteadOfGet")
    private fun setAdapter(allItemsArrayList: ArrayList<DataItem?>?) {
        val layoutManager: RecyclerView.LayoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        allRV.setLayoutManager(layoutManager)
        if(activity!=null) {
            allVerticalListAdapter = AllVerticalListAdapter(activity!!,allItemsArrayList)
            allRV.setAdapter(allVerticalListAdapter)
        }
    }
}
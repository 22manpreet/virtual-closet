package com.virtualcloset.app.activity

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.annotation.Nullable
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.virtualcloset.app.R
import com.virtualcloset.app.fragment.AllFragment
import com.virtualcloset.app.fragment.FavoriteFragment
import com.virtualcloset.app.utils.ALL_TAG
import com.virtualcloset.app.utils.FAVOURITE_TAG

class HomeActivity : BaseActivity() {

    @BindView(R.id.allTabTV)
    lateinit var allTabTV: TextView

    @BindView(R.id.favTabTV)
    lateinit var favTabTV: TextView

    @BindView(R.id.searchRL)
    lateinit var searchRL: RelativeLayout

    @BindView(R.id.profileRL)
    lateinit var profileRL: RelativeLayout
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        ButterKnife.bind(mActivity)
        setUpFirstFragment()
    }
    @OnClick(
        R.id.allTabTV,
        R.id.favTabTV,
        R.id.searchRL,
        R.id.profileRL
    )
    fun onViewClicked(view: View){
        when(view.id) {
            R.id.allTabTV -> performAllClick()
            R.id.favTabTV -> performFavouriteClick()
            R.id.searchRL -> performSearchClick()
            R.id.profileRL -> performProfileClick()
        }
    }

    private fun performProfileClick() {
        val i = Intent(mActivity, ProfileActivity::class.java)
        startActivity(i)
    }

    private fun performSearchClick() {
        val i = Intent(mActivity, SearchActivity::class.java)
        startActivity(i)
    }

    private fun setUpFirstFragment() {
        performAllClick()
    }

    private fun performAllClick() {
        tabSelection(0)
        switchFragment(AllFragment(), ALL_TAG, false, null)
    }

    private fun performFavouriteClick() {
        tabSelection(1)
        switchFragment(FavoriteFragment(), FAVOURITE_TAG, false, null)
    }


    @SuppressLint("ResourceAsColor", "NewApi")
    private fun tabSelection(mPos: Int) {
        allTabTV.background=getDrawable(R.drawable.tab_fav_bg)
        favTabTV.background=getDrawable(R.drawable.tab_fav_bg)
        allTabTV.setTextColor(getColor(R.color.black))
        favTabTV.setTextColor(getColor(R.color.black))
        when(mPos){
            0 ->{
                allTabTV.background=getDrawable(R.drawable.tab_bg)
                allTabTV.setTextColor(getColor(R.color.white))
                favTabTV.setTextColor(getColor(R.color.black))
            }
            1 ->{
                favTabTV.background=getDrawable(R.drawable.tab_bg)
                favTabTV.setTextColor(getColor(R.color.white))
               allTabTV.setTextColor(getColor(R.color.black))
            }
        }
    }
    override fun onActivityResult(requestCode: Int, resultCode: Int, @Nullable data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        for (fragment in supportFragmentManager.fragments) {
            fragment.onActivityResult(requestCode, resultCode, data)
        }
    }

}
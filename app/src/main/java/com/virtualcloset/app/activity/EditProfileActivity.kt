package com.virtualcloset.app.activity

import android.Manifest
import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.icu.number.NumberFormatter.with
import android.icu.number.NumberRangeFormatter.with
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.Window
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick

import com.bumptech.glide.Glide

import com.google.android.material.bottomsheet.BottomSheetDialog

import com.virtualcloset.app.Model.EditProfileModel

import com.virtualcloset.app.R
import com.virtualcloset.app.Retrofit.RetrofitClient
import com.virtualcloset.app.utils.AppPrefrences

import com.virtualcloset.app.utils.IMAGE
import com.virtualcloset.app.utils.NAME
import de.hdodenhof.circleimageview.CircleImageView
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException
import java.util.HashMap




class EditProfileActivity : BaseActivity() {
    @BindView(R.id.backRL)
    lateinit var backRL: ImageView

    @BindView(R.id.editProfileCameraIV)
    lateinit var editProfileCameraIV: ImageView

    @BindView(R.id.editProfileIV)
    lateinit var editProfileIV: CircleImageView

    @BindView(R.id.saveTV)
    lateinit var saveTV: TextView

    @BindView(R.id.nameET)
    lateinit var nameET: EditText

    @BindView(R.id.editEmailET)
    lateinit var editEmailET: EditText

    private val SELECT_IMAGE = 1
    val REQUEST_CODE_GALLERY = 100
    var mBitmap: Bitmap? = null

    private val REQUEST_PERMISSION = 100
    private val REQUEST_IMAGE_CAPTURE = 1
    private val REQUEST_PICK_IMAGE = 2

    var name:String?=null
    var email:String?=null
    var image:String?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_profile)
        ButterKnife.bind(mActivity)
       name= intent.getStringExtra("name")
        email=intent.getStringExtra("email")
        image=intent.getStringExtra("image")
        nameET.setText(name)
        editEmailET.setText(email)

        if(image == "" || image == null){

        }
        else{
            Glide.with(mActivity)
                .load(image)
                .placeholder(R.drawable.ic_placeholder)
                .error(R.drawable.ic_placeholder)
                .into(editProfileIV)
        }

    }

    override fun onResume() {
        super.onResume()
        checkCameraPermission()
    }
    private fun checkCameraPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
            != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                arrayOf(Manifest.permission.CAMERA),
                REQUEST_PERMISSION)
        }
    }
    @OnClick(
        R.id.backRL,R.id.editProfileCameraIV,R.id.saveTV
    )
    fun onViewClicked(view: View){
        when(view.id) {
            R.id.backRL -> onBackPressed()
            R.id.editProfileCameraIV -> performCameraClick()
            R.id.saveTV -> performOnSaveClick()
        }
    }

    private fun performOnSaveClick() {
        if(isValidate()){
            if (isNetworkAvailable(mActivity)) {
                executeEditProfileRequest()
            }
            else{
                showToast(mActivity, getString(R.string.internet_connection_error))
            }


         //   showBackAlertDialog(mActivity,getString(R.string.profile_updated_successfully))
        }
    }

    private fun executeEditProfileRequest() {
        var mMultipartBody: MultipartBody.Part? = null
        if (mBitmap != null) {
            val requestFile1: RequestBody? = convertBitmapToByteArrayUncompressed(mBitmap!!)?.let {
                RequestBody.create(
                    "multipart/form-data".toMediaTypeOrNull(),
                    it
                )
            }
            mMultipartBody = requestFile1?.let {
                MultipartBody.Part.createFormData(
                    "image",
                    getAlphaNumericString()!!.toString() + ".png",
                    it
                )
            }
        }
        val headers: MutableMap<String, String> = HashMap()
        headers["Token"] = getAuthToken()

        val name: RequestBody = nameET.text.toString().trim()
            .toRequestBody("multipart/form-data".toMediaTypeOrNull())
        val email: RequestBody = editEmailET.text.toString().trim()
            .toRequestBody("multipart/form-data".toMediaTypeOrNull())

        showProgressDialog(mActivity)
        val call = RetrofitClient.apiInterface.EditProfileDataRequest(headers,name,email,mMultipartBody)

        call.enqueue(object : Callback<EditProfileModel> {
            override fun onFailure(call: Call<EditProfileModel>, t: Throwable) {
                Log.e(TAG, t.message.toString())
                dismissProgressDialog()
            }
            override fun onResponse(call: Call<EditProfileModel>, response: Response<EditProfileModel>) {
                Log.e(TAG, response.body().toString())
                dismissProgressDialog()
                var  mEditProfileModel = response.body()
                if (mEditProfileModel!!.status == 1) {
                    AppPrefrences().writeString(mActivity, NAME,  mEditProfileModel.data!!.name)
                    AppPrefrences().writeString(mActivity, IMAGE, mEditProfileModel.data!!.image)
                    showToast(mActivity,getString(R.string.profile_updated_successfully))
                    finish()
                }
                else if(mEditProfileModel.status == 0){
                 //   showToast(mActivity,mEditProfileModel.message)
                    if(mEditProfileModel.message.equals("Unautorized request, please login again.")){
                        var intent=Intent(mActivity,SignInActivity::class.java)
                        startActivity(intent)
                    }
                }
                else {
                    showAlertDialog(mActivity, getString(R.string.internal_server_error))
                }
            }
        })
    }

    /*
  *
  *  Alert Dialog
  * */
        fun showBackAlertDialog(mActivity: Activity?, strMessage: String?) {
            val alertDialog = Dialog(mActivity!!)
            alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
            alertDialog.setContentView(R.layout.dialog_alert)
            alertDialog.setCanceledOnTouchOutside(false)
            alertDialog.setCancelable(false)
            alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            // set the custom dialog components - text, image and button
            val txtMessageTV = alertDialog.findViewById<TextView>(R.id.txtMessageTV)
            val btnDismiss = alertDialog.findViewById<TextView>(R.id.btnDismiss)
            txtMessageTV.text = strMessage
            btnDismiss.setOnClickListener {
                alertDialog.dismiss()
                onBackPressed()
                finish()
            }
            alertDialog.show()
        }

    // - - Validations for Sign In fields
    private fun isValidate(): Boolean {
        var flag = true
        when {
            nameET.text.toString().trim { it <= ' ' } == "" -> {
                showAlertDialog(mActivity, getString(R.string.please_enter_name))
                flag = false
            }

        }
        return flag
    }
    private fun performCameraClick() {
        showBottomSheet()
    }
    private fun showBottomSheet() {
        val view: View = LayoutInflater.from(mActivity).inflate(R.layout.layout_bottom_sheet, null)

        var dialogPrivacy: BottomSheetDialog? = mActivity.let { BottomSheetDialog(it, R.style.BottomSheetDialog) }
        dialogPrivacy!!.setContentView(view)
        dialogPrivacy.setCanceledOnTouchOutside(true)
        //disabling the drag down of sheet
        dialogPrivacy.setCancelable(true)
        //cancel button click

        val txtCameraTV: TextView? = dialogPrivacy.findViewById(R.id.txtCameraTV)
        val txtGalleryTV: TextView? = dialogPrivacy.findViewById(R.id.txtGalleryTV)


        txtCameraTV?.setOnClickListener {
            val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            startActivityForResult(intent,SELECT_IMAGE)

            dialogPrivacy.dismiss()
        }
        txtGalleryTV?.setOnClickListener {
            val intent = Intent(Intent.ACTION_PICK)
            intent.type="image/*"
            startActivityForResult(intent, REQUEST_CODE_GALLERY)
            dialogPrivacy.dismiss()
        }

        val cancelTV: TextView? = dialogPrivacy.findViewById(R.id.btnCancelTV)
        cancelTV?.setOnClickListener {
            dialogPrivacy.dismiss()
        }
        dialogPrivacy.show()
    }

    // Override this method too
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == SELECT_IMAGE) {
            if (resultCode == RESULT_OK) {
                if (data != null) {
                    try {
                        mBitmap =data.getParcelableExtra("data")
                        editProfileIV.setImageBitmap(mBitmap)
                    } catch (e: IOException) {
                        e.printStackTrace()
                    }
                }
            } else if (resultCode == RESULT_CANCELED) {
                // Toast.makeText(mActivity, "Cancel", Toast.LENGTH_SHORT).show()
            }
        }
        if (resultCode == Activity.RESULT_OK && requestCode == REQUEST_CODE_GALLERY){
            val selectedMediaUri: Uri = data!!.data!!
            //handle image
            try{
                //         mBitmap = data?.getParcelableExtra("data")
                mBitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), data.data)
                editProfileIV.setImageBitmap(mBitmap)
            //    editProfileIV.setImageURI(selectedMediaUri) // handle chosen image
            }catch (e: IOException) {
                e.printStackTrace()
            }
        }
    }
}



package com.virtualcloset.app.activity

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.View
import android.widget.*
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.virtualcloset.app.Interface.PaginationInquiriesInterface
import com.virtualcloset.app.Model.FavDetailDataItem
import com.virtualcloset.app.Model.GetallitemfavdetailsbycategoryModel
import com.virtualcloset.app.R
import com.virtualcloset.app.Retrofit.RetrofitClient
import com.virtualcloset.app.adapter.FavSeeMoreAdapter
import com.virtualcloset.app.adapter.SeeMoreAdapter
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.lang.Exception
import java.util.HashMap

class FavSeeMore : BaseActivity(), PaginationInquiriesInterface {
    @BindView(R.id.backRL)
    lateinit var backRL: ImageView

    @BindView(R.id.swipeToRefresh)
    lateinit var swipeToRefresh: SwipeRefreshLayout

    @BindView(R.id.nameTV)
    lateinit var nameTV: TextView

    @BindView(R.id.gridView)
    lateinit var gridView: GridView

    var seeMoreAdapter: FavSeeMoreAdapter? = null


    var FavDetailsItemArrayList: ArrayList<FavDetailDataItem?>? = ArrayList()
    var tempArrayList: ArrayList<FavDetailDataItem?>? = ArrayList()

    var catgoryName:String?=null
    var category_id : String?=null

    // pagination aparms
    var strLastPage: String? = "FALSE"
    var page_no = 1
    var mInterfaceData: PaginationInquiriesInterface? = null
    var isSwipeRefresh = false
    var mProgressBar: ProgressBar? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_fav_see_more)
        ButterKnife.bind(mActivity)


        category_id =intent.getStringExtra("category_id")
        catgoryName= intent.getStringExtra("category_name")
        nameTV.setText(catgoryName!!.substring(0, 1).toUpperCase() + catgoryName!!.substring(1).toLowerCase())
        mInterfaceData = this

    }

    override fun onResume() {
        super.onResume()
        //Restrict Double Click
        setAdapter()
        if (isNetworkAvailable(mActivity)) {
            FavDetailsItemArrayList!!.clear()
            tempArrayList!!.clear()
            page_no = 1
            executeGetAllItemDetailByCategoryIdRequest()
        } else showToast(mActivity, getString(R.string.internet_connection_error))
    }

    private fun executeGetAllItemDetailByCategoryIdRequest() {
        tempArrayList!!.clear();
        if (page_no == 1) {
            if (isSwipeRefresh) {
            } else {
                showProgressDialog(mActivity)
            }

        } else if (page_no > 1) {
            if(mProgressBar!=null)
            {
                mProgressBar!!.setVisibility(View.GONE)
            }

        }

        val headers: MutableMap<String, String> = HashMap()
        headers["Token"] = getAuthToken()

        val category_id: RequestBody = category_id!!.toRequestBody("multipart/form-data".toMediaTypeOrNull())
        val pageNo: RequestBody = page_no.toString().toRequestBody("multipart/form-data".toMediaTypeOrNull())
        val per_page: RequestBody = "20".toRequestBody("multipart/form-data".toMediaTypeOrNull())

        //    showProgressDialog(mActivity)
        val call = RetrofitClient.apiInterface.GetallitemfavdetailsbycategoryDataRequest(
            headers,
            category_id,pageNo,per_page)

        call.enqueue(object : Callback<GetallitemfavdetailsbycategoryModel> {
            override fun onFailure(call: Call<GetallitemfavdetailsbycategoryModel>, t: Throwable) {
                Log.e(TAG, t.message.toString())
                dismissProgressDialog()
            }
            override fun onResponse(call: Call<GetallitemfavdetailsbycategoryModel>, response: Response<GetallitemfavdetailsbycategoryModel>) {
                var  getAllItemDetailByCategoryIdModel = response.body()
                if (getAllItemDetailByCategoryIdModel!!.status == 1) {
                    if (page_no == 1) {
                        dismissProgressDialog()
                    }
                    try {
                        if (isSwipeRefresh) {
                            swipeToRefresh.setEnabled(false)
                        }
                        parseResponse(response)
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }

                    Log.e(TAG, response.body().toString())

                }


//                dismissProgressDialog()
//                var  getAllItemDetailByCategoryIdModel = response.body()
//                if (getAllItemDetailByCategoryIdModel!!.status == 1) {
//                    DetailsItemArrayList!!.addAll(getAllItemDetailByCategoryIdModel.data!!)
//                    setAdapter()
//                }
                else if(getAllItemDetailByCategoryIdModel!!.status == 0){
                   if(getAllItemDetailByCategoryIdModel.message.equals("Unautorized request, please login again.")){
                        var intent= Intent(mActivity,SignInActivity::class.java)
                        startActivity(intent)
                    }
                }
                else {
                    showAlertDialog(mActivity, getString(R.string.internal_server_error))
                }
            }
        })
    }

    private fun parseResponse(response: Response<GetallitemfavdetailsbycategoryModel>) {
        tempArrayList!!.clear()
        val mModel: GetallitemfavdetailsbycategoryModel? = response.body()
        if (mModel!!.status == 1) {
            //Conditions
            if (page_no == 1) {
                FavDetailsItemArrayList!!.addAll(mModel.data!!)
            } else if (page_no > 1) {
                tempArrayList!!.addAll(mModel.data!!)
            }
            if (tempArrayList!!.size > 0) {
                FavDetailsItemArrayList!!.addAll(tempArrayList!!)
            }
            if (page_no == 1) {
                setAdapter()
            } else {
                seeMoreAdapter!!.notifyDataSetChanged()
            }
        }
        else if (page_no == 1) {
            showToast(mActivity, mModel.message)
        }
    }

    @OnClick(
        R.id.backRL
    )
    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.backRL -> onBackPressed()
        }
    }

    private fun setAdapter() {
        seeMoreAdapter = FavSeeMoreAdapter(mActivity,FavDetailsItemArrayList,mInterfaceData)
        gridView.setNestedScrollingEnabled(false)
        gridView.adapter = seeMoreAdapter

        gridView.onItemClickListener = AdapterView.OnItemClickListener { _, _, position, _ ->
            val intent = Intent(mActivity, DetailActivity::class.java)
            intent.putExtra("category_name",catgoryName)
            intent.putExtra("item_id",FavDetailsItemArrayList!!.get(position)!!.itemId)
            startActivity(intent)
        }

    }
    override fun mPaginationInquiriesInterface(isLastScrolled: Boolean) {
        if (isLastScrolled == true) {
            if (mProgressBar != null) {
                mProgressBar!!.setVisibility(View.VISIBLE)
            }
            ++page_no

            Handler().postDelayed(Runnable {
                if (strLastPage == "FALSE") {
                    executeGetAllItemDetailByCategoryIdRequest()
                } else {
                    mProgressBar!!.setVisibility(View.GONE)
                }
            }, 1000)
        }
    }
}
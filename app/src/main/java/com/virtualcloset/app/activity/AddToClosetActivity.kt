package com.virtualcloset.app.activity

import android.R.attr
import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.media.ExifInterface
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.Window
import android.widget.*
import androidx.core.content.FileProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.facebook.drawee.backends.pipeline.Fresco
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.virtualcloset.app.Interface.RemoveImageInterface
import com.virtualcloset.app.Model.*
import com.virtualcloset.app.R
import com.virtualcloset.app.Retrofit.RetrofitClient
import com.virtualcloset.app.adapter.ImageListAdapter
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.HashMap

import com.zfdang.multiple_images_selector.ImagesSelectorActivity
import com.zfdang.multiple_images_selector.SelectorSettings
import java.io.*
import android.R.attr.bitmap

class AddToClosetActivity : BaseActivity() {

    @BindView(R.id.spinner)
    lateinit var spinner: Spinner

    @BindView(R.id.backRL)
    lateinit var backRL: ImageView

    @BindView(R.id.addNewCategory)
    lateinit var addNewCategory: TextView

    @BindView(R.id.uploadImagesRL)
    lateinit var uploadImagesRL: RelativeLayout

    @BindView(R.id.imagesRV)
    lateinit var imagesRV: RecyclerView

    @BindView(R.id.addTV)
    lateinit var addTV: TextView

    @BindView(R.id.descriptionET)
    lateinit var descriptionET: EditText

    lateinit var adapter: ArrayAdapter<String>
    var array: ArrayList<String?>? = ArrayList()
    var categoryIDArray: ArrayList<String?>? = ArrayList()
    private val SELECT_IMAGE = 1
    val REQUEST_CODE_GALLERY = 100
    var mBitmap: Bitmap? = null

    var imageListAdapter: ImageListAdapter? = null
    var imagesArrayList: ArrayList<Bitmap?>? = ArrayList()
    var PICK_IMAGE_MULTIPLE = 1
    var spinnerValue:String?=null
    private val REQUEST_CODE = 732
    var mMultileImagesAL = ArrayList<ImagesModel>()
    var mArrayUri: ArrayList<Uri>? = ArrayList<Uri>()
    var currentPhotoPath = ""
    var fileName: String = ""
    var selectedImage: Bitmap? = null
    private val permissions = arrayOf(
        "android.permission.WRITE_EXTERNAL_STORAGE",
        "android.permission.READ_EXTERNAL_STORAGE",
        "android.permission.CAMERA"
    )
    var mArrayList: ArrayList<String?>? = ArrayList()
    var mImagesArrayList: ArrayList<ImagesModel> = ArrayList()

    var CategoryArrayList: ArrayList<CatgoryDataItem?>? = ArrayList()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_to_closet)
        ButterKnife.bind(mActivity)
        Fresco.initialize(mActivity)
        categoryList()

        val requestCode = 200
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(permissions, requestCode)
        }

    }

    override fun onResume() {
        super.onResume()
        addTV.isClickable=true
    }
    private fun categoryList() {
        if (isNetworkAvailable(mActivity)) {
            executeGetAllCategoryRequest()
        } else {
            showToast(mActivity, getString(R.string.internet_connection_error))
        }
    }

    private fun executeGetAllCategoryRequest() {
        val headers: MutableMap<String, String> = HashMap()
        headers["Token"] = getAuthToken()
   //     showProgressDialog(mActivity)
        val call = RetrofitClient.apiInterface.GetAllCategoryDataRequest(headers)

        call.enqueue(object : Callback<GetAllCategoryModel> {
            override fun onFailure(call: Call<GetAllCategoryModel>, t: Throwable) {
                Log.e(TAG, t.message.toString())
   //             dismissProgressDialog()
            }
            override fun onResponse(call: Call<GetAllCategoryModel>, response: Response<GetAllCategoryModel>) {
                Log.e(TAG, response.body().toString())
    //            dismissProgressDialog()
                var  mGetAllCategoryModel = response.body()
                if (mGetAllCategoryModel!!.status == 1) {
                    CategoryArrayList=mGetAllCategoryModel.data
                    setSpinner()
                }
                else if(mGetAllCategoryModel.status == 0){
                  //  showToast(mActivity,mGetAllCategoryModel.message)
                    if(mGetAllCategoryModel.message.equals("Unautorized request, please login again.")){
                        var intent= Intent(mActivity,SignInActivity::class.java)
                        startActivity(intent)
                    }
                }
                else {
                    showAlertDialog(mActivity, getString(R.string.internal_server_error))
                }
            }
        })
    }

    @OnClick(
        R.id.backRL,R.id.addNewCategory,R.id.uploadImagesRL,R.id.addTV
    )
    fun onViewClicked(view: View){
        when(view.id) {
            R.id.backRL -> onBackPressed()
            R.id.addNewCategory -> onClickAddNewCategory()
            R.id.uploadImagesRL -> onUploadImagesClick()
            R.id.addTV -> performAddClick()
        }
    }

    @SuppressLint("InlinedApi")
    private fun performAddClick() {
        addTV.isClickable=false
        if(isValidate()){
            if (isNetworkAvailable(mActivity)) {
                for(i in 0..mMultileImagesAL.size-1) {
                    Log.e("images : ", mMultileImagesAL.get(i).image_name)
                    val bitmap = BitmapFactory.decodeFile(mMultileImagesAL.get(i).image_name)
//                    val bytearrayoutputstream = ByteArrayOutputStream()
//
//                    val resizedBitmap = Bitmap.createScaledBitmap(bitmap, 150, 150, false)
//
//                    resizedBitmap.compress(Bitmap.CompressFormat.JPEG, 50, bytearrayoutputstream)

                    imagesArrayList!!.add(bitmap)
                    //  imagesArrayList!!.add(mMultileImagesAL.get(i).image_name)
                }
                executeAddItemRequest()
            }
            else{
                showToast(mActivity, getString(R.string.internet_connection_error))
            }
        }
    }
    // - - Validations for Sign In fields
    private fun isValidate(): Boolean {
        var flag = true
        when {
            descriptionET.text.toString().trim { it <= ' ' } == "" -> {
                showAlertDialog(mActivity, getString(R.string.please_enter_description))
                flag = false
            }

            mMultileImagesAL!!.size == 0 -> {
                showAlertDialog(mActivity, getString(R.string.please_enter_atleast_one_closet_image))
                flag = false
            }
        }
        return flag
    }
    private fun executeAddItemRequest() {
        var mMultipartBody1: MultipartBody.Part? = null

        if(0 < imagesArrayList!!.size) {
            var bitmap1 = imagesArrayList!!.get(0)!!
            if (bitmap1 != null) {
                val requestFile1: RequestBody? =
                    convertBitmapToByteArrayUncompressed(bitmap1)?.let {
                        RequestBody.create(
                            "multipart/form-data".toMediaTypeOrNull(),
                            it
                        )
                    }
                mMultipartBody1 = requestFile1?.let {
                    MultipartBody.Part.createFormData(
                        "image1",
                        getAlphaNumericString()!!.toString() + ".png",
                        it
                    )
                }
            }
        }
        var mMultipartBody2: MultipartBody.Part? = null
        if(1 < imagesArrayList!!.size) {
            var bitmap2 = imagesArrayList!!.get(1)!!
            if (bitmap2 != null) {
                val requestFile1: RequestBody? =
                    convertBitmapToByteArrayUncompressed(bitmap2!!)?.let {
                        RequestBody.create(
                            "multipart/form-data".toMediaTypeOrNull(),
                            it
                        )
                    }
                mMultipartBody2 = requestFile1?.let {
                    MultipartBody.Part.createFormData(
                        "image2",
                        getAlphaNumericString()!!.toString() + ".png",
                        it
                    )
                }
            }
        }
        var mMultipartBody3: MultipartBody.Part? = null
        if(2 < imagesArrayList!!.size) {
            var bitmap3 = imagesArrayList!!.get(2)!!
            if (bitmap3 != null) {
                val requestFile1: RequestBody? =
                    convertBitmapToByteArrayUncompressed(bitmap3!!)?.let {
                        RequestBody.create(
                            "multipart/form-data".toMediaTypeOrNull(),
                            it
                        )
                    }
                mMultipartBody3 = requestFile1?.let {
                    MultipartBody.Part.createFormData(
                        "image3",
                        getAlphaNumericString()!!.toString() + ".png",
                        it
                    )
                }
            }
        }
        var mMultipartBody4: MultipartBody.Part? = null
        if(3 < imagesArrayList!!.size) {
            var bitmap4 = imagesArrayList!!.get(3)!!
            if (bitmap4 != null) {
                val requestFile1: RequestBody? =
                    convertBitmapToByteArrayUncompressed(bitmap4!!)?.let {
                        RequestBody.create(
                            "multipart/form-data".toMediaTypeOrNull(),
                            it
                        )
                    }
                mMultipartBody4 = requestFile1?.let {
                    MultipartBody.Part.createFormData(
                        "image4",
                        getAlphaNumericString()!!.toString() + ".png",
                        it
                    )
                }
            }
        }
        var mMultipartBody5: MultipartBody.Part? = null
        if(4 < imagesArrayList!!.size) {
            var bitmap5 = imagesArrayList!!.get(4)!!
            if (bitmap5 != null) {
                val requestFile1: RequestBody? =
                    convertBitmapToByteArrayUncompressed(bitmap5!!)?.let {
                        RequestBody.create(
                            "multipart/form-data".toMediaTypeOrNull(),
                            it
                        )
                    }
                mMultipartBody5 = requestFile1?.let {
                    MultipartBody.Part.createFormData(
                        "image5",
                        getAlphaNumericString()!!.toString() + ".png",
                        it
                    )
                }
            }
        }
        val headers: MutableMap<String, String> = HashMap()
        headers["Token"] = getAuthToken()

        val category_id: RequestBody = spinnerValue!!.toRequestBody("multipart/form-data".toMediaTypeOrNull())
        val description: RequestBody = descriptionET.text.toString().trim()
            .toRequestBody("multipart/form-data".toMediaTypeOrNull())

        showProgressDialog(mActivity)
        val call = RetrofitClient.apiInterface.AddItemsDataRequest(headers,category_id,description,mMultipartBody1,mMultipartBody2,mMultipartBody3,mMultipartBody4,mMultipartBody5)

        call.enqueue(object : Callback<AddItemsModel> {
            override fun onFailure(call: Call<AddItemsModel>, t: Throwable) {
                Log.e(TAG, t.message.toString())
                dismissProgressDialog()
            }
            override fun onResponse(call: Call<AddItemsModel>, response: Response<AddItemsModel>) {
                Log.e(TAG, response.body().toString())
                dismissProgressDialog()
                var  mAddItemsModel = response.body()
                if (mAddItemsModel!!.status == 1) {
                    val i = Intent(mActivity, HomeActivity::class.java)
                    startActivity(i)
                    finish()
                }
                else if(mAddItemsModel.status == 0){
                 //   showToast(mActivity,mAddItemsModel.message)
                    if(mAddItemsModel.message.equals("Unautorized request, please login again.")){
                        var intent= Intent(mActivity,SignInActivity::class.java)
                        startActivity(intent)
                    }
                }
                else {
                    showAlertDialog(mActivity, getString(R.string.internal_server_error))
                }
            }
        })
    }
    private fun onUploadImagesClick() {
        if (mMultileImagesAL.size < 5) {
            showBottomSheet()
        }
       else{
           showToast(mActivity,getString(R.string.you_can_select_only_5images))
       }
    }

    private fun onClickAddNewCategory() {
        AddNewCategory()
    }

    private fun setSpinner() {
        array!!.clear()
        categoryIDArray!!.clear()
        for(i in 0..CategoryArrayList!!.size-1) {
            var categoryName: String= CategoryArrayList!!.get(i)!!.categoryName!!
            var category_ID:String = CategoryArrayList!!.get(i)!!.categoryId!!
            array!!.add(categoryName!!.substring(0, 1).toUpperCase() + categoryName!!.substring(1).toLowerCase())
            categoryIDArray!!.add(category_ID)
        }

        adapter = ArrayAdapter(this, R.layout.spinner_item_selected, array!!)
        spinner.adapter = adapter
        spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>,
                view: View,
                position: Int,
                id: Long
            ) {
                spinnerValue=categoryIDArray!!.get(position)
              //  spinnerValue=parent.getItemAtPosition(position).toString()
       //         Toast.makeText(mActivity, parent.getItemAtPosition(position).toString(), Toast.LENGTH_LONG).show()
            }

            override fun onNothingSelected(parent: AdapterView<*>) {

            }
        }
    }
    private fun AddNewCategory() {
        val alertDialog = Dialog(mActivity)
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        alertDialog.setContentView(R.layout.edit_category_dialog)
        alertDialog.setCanceledOnTouchOutside(true)
        alertDialog.setCancelable(true)
        alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        var category = alertDialog.findViewById<EditText>(R.id.categoryET)
        val cancelTV = alertDialog.findViewById<TextView>(R.id.cancelTV)
        cancelTV.setOnClickListener {
            alertDialog.dismiss()
        }
        val addTV = alertDialog.findViewById<TextView>(R.id.addTV)
        addTV.setOnClickListener {
            alertDialog.dismiss()
            if(category.text.toString() != "") {
                if (isNetworkAvailable(mActivity)) {
                    executeAddCategoryRequest(category.text.toString().trim())
                } else {
                    showToast(mActivity, getString(R.string.internet_connection_error))
                }
            }
            else{
                showAlertDialog(mActivity,"Please enter Category.")
            }
        }
        alertDialog.show()
    }


    private fun executeAddCategoryRequest(category: String) {
        val headers: MutableMap<String, String> = HashMap()
        headers["Token"] = getAuthToken()

        val category_name: RequestBody =category
            .toRequestBody("multipart/form-data".toMediaTypeOrNull())
        val role: RequestBody ="1"
            .toRequestBody("multipart/form-data".toMediaTypeOrNull())
        val is_remove: RequestBody = "1"
            .toRequestBody("multipart/form-data".toMediaTypeOrNull())


        showProgressDialog(mActivity)
        val call = RetrofitClient.apiInterface.AddCategoryDataRequest(headers,category_name,role,is_remove)

        call.enqueue(object : Callback<AddCategoryModel> {
            override fun onFailure(call: Call<AddCategoryModel>, t: Throwable) {
                Log.e(TAG, t.message.toString())
                dismissProgressDialog()
            }
            override fun onResponse(call: Call<AddCategoryModel>, response: Response<AddCategoryModel>) {
                Log.e(TAG, response.body().toString())
                dismissProgressDialog()
                var  mAddCategoryModel = response.body()
                if (mAddCategoryModel!!.status == 1) {
                    categoryList()
                }
                else if(mAddCategoryModel.status == 0){
                   // showToast(mActivity,mAddCategoryModel.message)
                    if(mAddCategoryModel.message.equals("Unautorized request, please login again.")){
                        var intent= Intent(mActivity,SignInActivity::class.java)
                        startActivity(intent)
                    }
                }
                else {
                    showAlertDialog(mActivity, getString(R.string.internal_server_error))
                }
            }
        })
    }

    private fun showBottomSheet() {
        val view: View = LayoutInflater.from(mActivity).inflate(R.layout.layout_bottom_sheet, null)

        var dialogPrivacy: BottomSheetDialog? = mActivity.let { BottomSheetDialog(it, R.style.BottomSheetDialog) }
        dialogPrivacy!!.setContentView(view)
        dialogPrivacy.setCanceledOnTouchOutside(true)
        //disabling the drag down of sheet
        dialogPrivacy.setCancelable(true)
        //cancel button click

        val txtCameraTV: TextView? = dialogPrivacy.findViewById(R.id.txtCameraTV)
        val txtGalleryTV: TextView? = dialogPrivacy.findViewById(R.id.txtGalleryTV)


        txtCameraTV?.setOnClickListener {
//            val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
//            startActivityForResult(intent,SELECT_IMAGE)
//            dialogPrivacy.dismiss()
            val pictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            val file = imageFile // 1
            val uri: Uri
            uri = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) // 2
                FileProvider.getUriForFile(
                    mActivity, "com.virtualcloset.app",
                    file!!
                ) else Uri.fromFile(file) // 3
            pictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, uri) // 4
            if (pictureIntent != null) {
                startActivityForResult(pictureIntent, 0)
            }
            dialogPrivacy.dismiss()
        }
        txtGalleryTV?.setOnClickListener {
//            val intent = Intent(Intent.ACTION_PICK)
//            intent.type="image/*"
//            startActivityForResult(intent, REQUEST_CODE_GALLERY)
//            dialogPrivacy.dismiss()
            if (mMultileImagesAL.size < 5) {
                performMultiImgClick()
            }

            dialogPrivacy.hide()
        }

        val cancelTV: TextView? = dialogPrivacy.findViewById(R.id.btnCancelTV)
        cancelTV?.setOnClickListener {
            dialogPrivacy.dismiss()
        }
        dialogPrivacy.show()
    }
    private val imageFile: File?
        private get() {
            val imageFileName = "JPEG_" + System.currentTimeMillis() + "_"
            val storageDir = File(
                Environment.getExternalStoragePublicDirectory(
                    Environment.DIRECTORY_DCIM
                ), "Camera"
            )
            var file: File? = null
            try {
                file = File.createTempFile(
                    imageFileName, ".jpg", storageDir
                )
            } catch (e: IOException) {
                e.printStackTrace()
            }
            if (file != null && file.absolutePath != null) {
                currentPhotoPath = "file:" + file.absolutePath
            }
            return file
        }
    private fun performMultiImgClick() {

        // start multiple photos selector
        val intent = Intent(this, ImagesSelectorActivity::class.java)
        // show camera or not
        intent.putExtra(SelectorSettings.SELECTOR_SHOW_CAMERA, false)
        // max number of images to be selected

        if (mMultileImagesAL.size == 0) {
            intent.putExtra(SelectorSettings.SELECTOR_MAX_IMAGE_NUMBER, 5)
        } else if (mMultileImagesAL.size == 1) {
            intent.putExtra(SelectorSettings.SELECTOR_MAX_IMAGE_NUMBER, 4)
        } else if (mMultileImagesAL.size == 2) {
            intent.putExtra(SelectorSettings.SELECTOR_MAX_IMAGE_NUMBER, 3)
        }else if (mMultileImagesAL.size == 3) {
            intent.putExtra(SelectorSettings.SELECTOR_MAX_IMAGE_NUMBER, 2)
        } else if (mMultileImagesAL.size == 4) {
            intent.putExtra(SelectorSettings.SELECTOR_MAX_IMAGE_NUMBER, 1)
        }


        // min size of image which will be shown; to filter tiny images (mainly icons)
        intent.putExtra(SelectorSettings.SELECTOR_MIN_IMAGE_SIZE, 100)
        // pass current selected images as the initial value
        // intent.putStringArrayListExtra(SelectorSettings.SELECTOR_INITIAL_SELECTED_LIST, mArrayList);
        // start the selector
        startActivityForResult(intent, REQUEST_CODE)

    }

    // Override this method too
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 0 && resultCode == RESULT_OK) {
            val uri = Uri.parse(currentPhotoPath)
            uri?.let { showImage(it) }
        }
        else if (requestCode == 1 && resultCode == RESULT_OK && data != null) {
            val sourceUri = data.data // 1
            if (data != null) {
                showImage(sourceUri)
            }
        }
        else if (requestCode == REQUEST_CODE) {
            if (data != null) {
                mArrayList = data.getStringArrayListExtra(SelectorSettings.SELECTOR_RESULTS)!!
                assert(mArrayList != null)
                for (i in mArrayList!!.indices) {
                    val mModel = ImagesModel()
                    mModel.image_name = mArrayList!!.get(i)!!
                    mMultileImagesAL.add(mModel)
                }
                setAdapter()
            }
        }


    }

    private fun showImage(imageUri: Uri?) {
        Log.e(TAG, "IMAGEVALUE::$imageUri")
        fileName = getRealPathFromURI_API19(mActivity, imageUri!!)!!
        Log.e(TAG, "PATH::$fileName")
        var inputStream: InputStream? = null
        try {
            inputStream = FileInputStream(fileName)
        } catch (e: FileNotFoundException) {
            Log.e(TAG, "EXCEPTION::$e")
            e.printStackTrace()
        }
        if (inputStream != null) {
            selectedImage = BitmapFactory.decodeStream(inputStream)
            var ei: ExifInterface? = null
            try {
                ei = ExifInterface(fileName)
            } catch (e: IOException) {
                e.printStackTrace()
            }
            val orientation = ei!!.getAttributeInt(
                ExifInterface.TAG_ORIENTATION,
                ExifInterface.ORIENTATION_UNDEFINED
            )

            if (inputStream != null) {
                selectedImage = BitmapFactory.decodeStream(inputStream)
                val mImagesModel = ImagesModel()
                mImagesModel.image_name = fileName
                mMultileImagesAL.add(mImagesModel)
                setAdapter()
            }
        }
    }

    var mRemoveImageInterface = object : RemoveImageInterface {
        override fun onRemoveClick(pos: Int, type: String?) {
            mMultileImagesAL.removeAt(pos)
            imageListAdapter!!.notifyDataSetChanged()
        }
    }

    @SuppressLint("UseRequireInsteadOfGet")
    private fun setAdapter() {
        val layoutManager: RecyclerView.LayoutManager = LinearLayoutManager(mActivity, LinearLayoutManager.HORIZONTAL, false)
        imagesRV.setLayoutManager(layoutManager)
        imageListAdapter = ImageListAdapter(mActivity,mMultileImagesAL,mRemoveImageInterface)
            imagesRV.setAdapter(imageListAdapter)
    }
}
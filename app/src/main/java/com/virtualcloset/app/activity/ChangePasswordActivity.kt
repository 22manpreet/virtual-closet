package com.virtualcloset.app.activity

import android.content.Intent
import android.os.Bundle
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.util.Log
import android.view.View
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.virtualcloset.app.Model.ChangePasswordModel
import com.virtualcloset.app.R
import com.virtualcloset.app.Retrofit.RetrofitClient
import com.virtualcloset.app.utils.AppPrefrences
import com.virtualcloset.app.utils.PASSWORD
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.HashMap

class ChangePasswordActivity : BaseActivity() {
    @BindView(R.id.backRL)
    lateinit var backRL: ImageView

    @BindView(R.id.currentPasswordET)
    lateinit var currentPasswordET: EditText

    @BindView(R.id.newPasswordET)
    lateinit var newPasswordET: EditText

    @BindView(R.id.confirmPasswordET)
    lateinit var confirmPasswordET: EditText

    @BindView(R.id.current_pwd_btn)
    lateinit var current_pwd_btn: TextView

    @BindView(R.id.new_pwd_btn)
    lateinit var new_pwd_btn: TextView

    @BindView(R.id.confirm_pwd_btn)
    lateinit var confirm_pwd_btn: TextView

    @BindView(R.id.saveTV)
    lateinit var saveTV: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_change_password)
        ButterKnife.bind(mActivity)
    }
    @OnClick(
        R.id.backRL,R.id.saveTV,R.id.current_pwd_btn,R.id.new_pwd_btn,R.id.confirm_pwd_btn
    )
    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.backRL -> onBackPressed()
            R.id.saveTV -> performSaveClick()
            R.id.current_pwd_btn ->clickOnshowHideCurrentPwd()
            R.id.new_pwd_btn -> clickOnShowHideNewPwd()
            R.id.confirm_pwd_btn -> clickOnShowHideConfirmPwd()
        }
    }

    private fun clickOnShowHideConfirmPwd() {
        if(confirmPasswordET.getTransformationMethod().equals(PasswordTransformationMethod.getInstance())){
            confirm_pwd_btn.background=getDrawable(R.drawable.ic_show_pwd)

            //Show Password
            confirmPasswordET.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
        }
        else{
            confirm_pwd_btn.background=getDrawable(R.drawable.ic_hide_pwd)

            //Hide Password
            confirmPasswordET.setTransformationMethod(PasswordTransformationMethod.getInstance());

        }
    }

    private fun clickOnShowHideNewPwd() {
        if(newPasswordET.getTransformationMethod().equals(PasswordTransformationMethod.getInstance())){
            new_pwd_btn.background=getDrawable(R.drawable.ic_show_pwd)

            //Show Password
            newPasswordET.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
        }
        else{
            new_pwd_btn.background=getDrawable(R.drawable.ic_hide_pwd)

            //Hide Password
            newPasswordET.setTransformationMethod(PasswordTransformationMethod.getInstance());

        }
    }

    private fun clickOnshowHideCurrentPwd() {
        if(currentPasswordET.getTransformationMethod().equals(PasswordTransformationMethod.getInstance())){
            current_pwd_btn.background=getDrawable(R.drawable.ic_show_pwd)

            //Show Password
            currentPasswordET.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
        }
        else{
            current_pwd_btn.background=getDrawable(R.drawable.ic_hide_pwd)

            //Hide Password
            currentPasswordET.setTransformationMethod(PasswordTransformationMethod.getInstance());

        }
    }
    private fun performSaveClick() {
        if(isValidate()){
            if (isNetworkAvailable(mActivity)) {
                executeChangePwdRequest()
            }
            else{
                showToast(mActivity, getString(R.string.internet_connection_error))
            }
        }
    }

    private fun executeChangePwdRequest() {
        val headers: MutableMap<String, String> = HashMap()
        headers["Token"] = getAuthToken()

        val current_pwd: RequestBody = currentPasswordET.text.toString().trim()
            .toRequestBody("multipart/form-data".toMediaTypeOrNull())
        val new_pwd: RequestBody = newPasswordET.text.toString().trim()
            .toRequestBody("multipart/form-data".toMediaTypeOrNull())

        showProgressDialog(mActivity)
        val call = RetrofitClient.apiInterface.ChangePasswordDataRequest(headers,current_pwd,new_pwd)

        call.enqueue(object : Callback<ChangePasswordModel> {
            override fun onFailure(call: Call<ChangePasswordModel>, t: Throwable) {
                Log.e(TAG, t.message.toString())
                dismissProgressDialog()
            }
            override fun onResponse(call: Call<ChangePasswordModel>, response: Response<ChangePasswordModel>) {
                Log.e(TAG, response.body().toString())
                dismissProgressDialog()
                var  mChangePasswordModel = response.body()
                if (mChangePasswordModel!!.status == 1) {
                    AppPrefrences().writeString(mActivity, PASSWORD,  newPasswordET.text.toString())
                    showToast(mActivity,mChangePasswordModel!!.message)
                    onBackPressed()
                }
                else if(mChangePasswordModel!!.status == 0){
                  //  showToast(mActivity,mChangePasswordModel!!.message)
                    if(mChangePasswordModel.message.equals("Unautorized request, please login again.")){
                        var intent= Intent(mActivity,SignInActivity::class.java)
                        startActivity(intent)
                    }
                }
                else {
                    showAlertDialog(mActivity, getString(R.string.internal_server_error))
                }
            }
        })
    }

    // - - Validations for Sign In fields
    private fun isValidate(): Boolean {
        var flag = true
        when {
            currentPasswordET.text.toString().trim { it <= ' ' } == "" -> {
                showAlertDialog(mActivity, getString(R.string.please_enter_current_password))
                flag = false
            }

            currentPasswordET.text.toString() != getPassword() ->{
                showAlertDialog(mActivity, getString(R.string.old_password_do_not_match_pleaseenter_the_correct_password))
                flag = false
            }

            newPasswordET.text.toString().trim { it <= ' ' } == "" -> {
                showAlertDialog(mActivity, getString(R.string.please_enter_new_password))
                flag = false
            }

            newPasswordET.text.toString().length < 6 -> {
                showAlertDialog(mActivity, getString(R.string.please_enter_minimum_6_))
                flag = false
            }

            confirmPasswordET.text.toString().trim { it <= ' ' } == "" -> {
                showAlertDialog(mActivity, getString(R.string.please_enter_confirm_password))
                flag = false
            }

            confirmPasswordET.text.toString().trim { it <= ' ' } != newPasswordET.text.toString() -> {
                showAlertDialog(mActivity, getString(R.string.please_enter_same_new_password_and_confirm_password))
                flag = false
            }
        }
        return flag
    }
}

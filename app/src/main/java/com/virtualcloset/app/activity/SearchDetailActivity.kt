package com.virtualcloset.app.activity

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ImageView
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.virtualcloset.app.Model.GetAllItemBySearchModel
import com.virtualcloset.app.Model.GetSearchDataItem
import com.virtualcloset.app.R
import com.virtualcloset.app.Retrofit.RetrofitClient
import com.virtualcloset.app.adapter.SearchListDetailAdapter
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.HashMap

class SearchDetailActivity : BaseActivity() {

    var search:String?=null
    @BindView(R.id.backRL)
    lateinit var backRL: ImageView

    @BindView(R.id.search_detailRV)
    lateinit var search_detailRV:RecyclerView

    var searchListDetailAdapter: SearchListDetailAdapter? = null

    var GetDetailArrayList: ArrayList<GetSearchDataItem?>? = ArrayList()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search_detail)
        ButterKnife.bind(mActivity)

        search = intent.getStringExtra("search")
    }

    override fun onResume() {
        super.onResume()
        if (isNetworkAvailable(mActivity)) {
            executeGetAllItemBySearchRequest()
        } else {
            showToast(mActivity, getString(R.string.internet_connection_error))
        }
    }
    @OnClick(
        R.id.backRL
    )
    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.backRL -> onBackPressed()
        }
    }
    private fun executeGetAllItemBySearchRequest() {
        val headers: MutableMap<String, String> = HashMap()
        headers["Token"] = getAuthToken()

        val pageNo: RequestBody = "1".toRequestBody("multipart/form-data".toMediaTypeOrNull())
        val per_page: RequestBody = "10".toRequestBody("multipart/form-data".toMediaTypeOrNull())
        val search: RequestBody = search!!.toRequestBody("multipart/form-data".toMediaTypeOrNull())

        showProgressDialog(mActivity)
        val call = RetrofitClient.apiInterface.GetAllItemBySearchDataRequest(headers,pageNo,per_page,search)

        call.enqueue(object : Callback<GetAllItemBySearchModel> {
            override fun onFailure(call: Call<GetAllItemBySearchModel>, t: Throwable) {
                Log.e(TAG, t.message.toString())
                dismissProgressDialog()
            }
            override fun onResponse(call: Call<GetAllItemBySearchModel>, response: Response<GetAllItemBySearchModel>) {
                Log.e(TAG, response.body().toString())
                dismissProgressDialog()
                var mGetAllItemBySearchModel = response.body()!!
                if (mGetAllItemBySearchModel!!.status == 1) {
                    GetDetailArrayList=mGetAllItemBySearchModel.data
                    setAdapter()
                }
                else if(mGetAllItemBySearchModel!!.status == 0){
                  //  showToast(mActivity,mGetAllItemBySearchModel!!.message)
                    if(mGetAllItemBySearchModel.message.equals("Unautorized request, please login again.")){
                        var intent= Intent(mActivity,SignInActivity::class.java)
                        startActivity(intent)
                    }
                }
                else {
                    showAlertDialog(mActivity, getString(R.string.internal_server_error))
                }
            }
        })
    }

    @SuppressLint("UseRequireInsteadOfGet")
    private fun setAdapter() {
        val layoutManager: RecyclerView.LayoutManager = GridLayoutManager(mActivity,2)
        searchListDetailAdapter = SearchListDetailAdapter(mActivity,GetDetailArrayList)
        search_detailRV.setLayoutManager(layoutManager)
        search_detailRV.setAdapter(searchListDetailAdapter)
    }
}


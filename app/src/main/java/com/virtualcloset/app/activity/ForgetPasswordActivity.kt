package com.virtualcloset.app.activity

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.EditText
import android.widget.RelativeLayout
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.virtualcloset.app.Model.ForgotPasswordModel
import com.virtualcloset.app.R
import com.virtualcloset.app.Retrofit.RetrofitClient
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ForgetPasswordActivity : BaseActivity() {

    @BindView(R.id.ic_back)
    lateinit var ic_back: RelativeLayout

    @BindView(R.id.editEmailET)
    lateinit var editEmailET: EditText

    @BindView(R.id.submitTV)
    lateinit var submitTV: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_forget_password)
        ButterKnife.bind(mActivity)
    }

    @OnClick(
        R.id.submitTV,R.id.ic_back

    )
    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.submitTV -> performSubmitClick()
            R.id.ic_back -> onBackPressed()
        }
    }

    private fun performSubmitClick() {
        if(isValidate()) {
            if (isNetworkAvailable(mActivity)) {
                executeForgotPasswordRequest()
            } else {
                showToast(mActivity, getString(R.string.internet_connection_error))
            }
        }
    }

    private fun executeForgotPasswordRequest() {
        val email: RequestBody = editEmailET.text.toString().trim()
            .toRequestBody("multipart/form-data".toMediaTypeOrNull())
        showProgressDialog(mActivity)
        val call = RetrofitClient.apiInterface.ForgotPasswordDataRequest(email)

        call.enqueue(object : Callback<ForgotPasswordModel> {
            override fun onFailure(call: Call<ForgotPasswordModel>, t: Throwable) {
                Log.e(TAG, t.message.toString())
                dismissProgressDialog()
            }
            override fun onResponse(call: Call<ForgotPasswordModel>, response: Response<ForgotPasswordModel>) {
                Log.e(TAG, response.body().toString())
                dismissProgressDialog()
                var  mForgotPasswordModel = response.body()
                if (mForgotPasswordModel!!.status == 1) {
                    showAlertDialog(mActivity, mForgotPasswordModel.message)
                }
                else if(mForgotPasswordModel!!.status == 0){
                    if(mForgotPasswordModel.message.equals("Unautorized request, please login again.")){
                        var intent= Intent(mActivity,SignInActivity::class.java)
                        startActivity(intent)
                    }
                }
                else {
                    showAlertDialog(mActivity, mForgotPasswordModel.message)
                }
            }
        })
    }

    // - - Validations for Sign In fields
    private fun isValidate(): Boolean {
        var flag = true
        when {
            editEmailET.text.toString().trim { it <= ' ' } == "" -> {
                showAlertDialog(mActivity, getString(R.string.please_enter_email))
                flag = false
            }
        }
        return flag
    }
}
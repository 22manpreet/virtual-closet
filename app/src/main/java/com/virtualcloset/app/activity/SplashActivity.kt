package com.virtualcloset.app.activity

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.ImageView
import butterknife.BindView
import com.virtualcloset.app.Model.SubscriptionStatusModel
import com.virtualcloset.app.R
import com.virtualcloset.app.Retrofit.RetrofitClient
import com.virtualcloset.app.utils.SPLASH_TIME_OUT
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.HashMap

class SplashActivity : BaseActivity() {

    @BindView(R.id.splashIV)
    lateinit var splashIV: ImageView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        setUpSplash()
    }

    private fun setUpSplash() {
        val mThread = object : Thread() {
            override fun run() {
                sleep(SPLASH_TIME_OUT.toLong())
                if (isUserLogin()) {
                    executeCheckSubscriptionPlan()
//                    val i = Intent(mActivity, HomeActivity::class.java)
//                    startActivity(i)
//                    finish()
                }
                else{
                    val i = Intent(mActivity, SignInActivity::class.java)
                    startActivity(i)
                    finish()
                }
            }
        }
        mThread.start()
    }
    private fun executeCheckSubscriptionPlan() {
        val headers: MutableMap<String, String> = HashMap()
        headers["Token"] = getAuthToken()
        val call = RetrofitClient.apiInterface.SubscriptionStatusDataRequest(headers)

        call.enqueue(object : Callback<SubscriptionStatusModel> {
            override fun onFailure(call: Call<SubscriptionStatusModel>, t: Throwable) {
                Log.e(TAG, t.message.toString())
                dismissProgressDialog()
            }
            override fun onResponse(call: Call<SubscriptionStatusModel>, response: Response<SubscriptionStatusModel>) {
                Log.e(TAG, response.body().toString())
                dismissProgressDialog()
                var  subscriptionStatusModel = response.body()
                if (subscriptionStatusModel!!.status == 1) {
                    val i = Intent(mActivity, HomeActivity::class.java)
                    startActivity(i)
                    finish()
                }
                else if(subscriptionStatusModel!!.status == 0){
                    if(subscriptionStatusModel!!.message.equals("Unautorized request, please login again.")){
                        val i = Intent(mActivity, SignInActivity::class.java)
                        startActivity(i)
                        finish()
                    }
                    else {
                        val i = Intent(mActivity, FreeTrialActivity::class.java)
                        startActivity(i)
                        finish()
                    }
                }
                else if(subscriptionStatusModel!!.status == 7){
                    val i = Intent(mActivity, HomeActivity::class.java)
                    startActivity(i)
                    finish()
                }
                else {
                    showAlertDialog(mActivity, subscriptionStatusModel!!.message)
                }
            }
        })
    }
}
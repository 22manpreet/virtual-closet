package com.virtualcloset.app.activity

import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.util.Log
import android.view.View
import android.view.Window
import android.widget.CheckBox
import android.widget.EditText
import android.widget.RelativeLayout
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.virtualcloset.app.Model.SignUpModel
import com.virtualcloset.app.R
import com.virtualcloset.app.Retrofit.RetrofitClient
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class SignUpActivity : BaseActivity() {

    @BindView(R.id.nameET)
    lateinit var nameET: EditText

    @BindView(R.id.editEmailET)
    lateinit var editEmailET: EditText

    @BindView(R.id.passwordET)
    lateinit var passwordET: EditText

    @BindView(R.id.confirmPasswordET)
    lateinit var confirmPasswordET: EditText

    @BindView(R.id.imgAgreeCV)
    lateinit var imgAgreeCV: CheckBox

    @BindView(R.id.signupTV)
    lateinit var signupTV: TextView

    @BindView(R.id.Confirm_toggle_btn)
    lateinit var Confirm_toggle_btn: TextView

    @BindView(R.id.password_toggle_btn)
    lateinit var password_toggle_btn: TextView

    @BindView(R.id.ic_back)
    lateinit var ic_back: RelativeLayout

    @BindView(R.id.terms_conditionsTV)
    lateinit var terms_conditionsTV: TextView
    private var checkUncheck: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up)
        ButterKnife.bind(mActivity)
       println("device_name : "+getDeviceName())
    }

    @OnClick(
        R.id.signupTV,
        R.id.password_toggle_btn,
        R.id.Confirm_toggle_btn,
        R.id.imgAgreeCV,
        R.id.ic_back,
        R.id.terms_conditionsTV
    )
    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.signupTV -> performSignUpClick()
            R.id.password_toggle_btn -> clickOnshowHidePwd()
            R.id.Confirm_toggle_btn -> clickOnshowHideConfirmPwd()
            R.id.imgAgreeCV -> performAgreeClick()
            R.id.ic_back -> onBackPressed()
            R.id.terms_conditionsTV -> performTersmAndConditions()
        }
    }

    private fun performTersmAndConditions() {
        val i = Intent(mActivity, TermsAndConditionsActivity::class.java)
        startActivity(i)
    }

    private fun clickOnshowHideConfirmPwd() {
        if(confirmPasswordET.getTransformationMethod().equals(PasswordTransformationMethod.getInstance())){
            Confirm_toggle_btn.background=getDrawable(R.drawable.ic_show_pwd)
            //Show Password
            confirmPasswordET.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
        }
        else{
            Confirm_toggle_btn.background=getDrawable(R.drawable.ic_hide_pwd)
            //Hide Password
            confirmPasswordET.setTransformationMethod(PasswordTransformationMethod.getInstance());

        }
    }

    private fun performSignUpClick() {
        if(isValidate()) {
            if (isNetworkAvailable(mActivity)) {
                executeSignUpRequest()
            } else {
                showToast(mActivity, getString(R.string.internet_connection_error))
            }
        }
    }

    private fun executeSignUpRequest() {
        val name: RequestBody = nameET.text.toString().trim()
            .toRequestBody("multipart/form-data".toMediaTypeOrNull())
        val email: RequestBody = editEmailET.text.toString().trim()
            .toRequestBody("multipart/form-data".toMediaTypeOrNull())
        val password: RequestBody =  passwordET.text.toString().trim()
            .toRequestBody("multipart/form-data".toMediaTypeOrNull())
        val device_name: RequestBody = getDeviceName()
            .toRequestBody("multipart/form-data".toMediaTypeOrNull())
        val disable: RequestBody =  "1"
            .toRequestBody("multipart/form-data".toMediaTypeOrNull())

        showProgressDialog(mActivity)
        val call = RetrofitClient.apiInterface.SignUpDataRequest(
            name,
            email,
            password,
            device_name,
            disable
        )

        call.enqueue(object : Callback<SignUpModel> {
            override fun onFailure(call: Call<SignUpModel>, t: Throwable) {
                Log.e(TAG, t.message.toString())
                dismissProgressDialog()
            }
            override fun onResponse(call: Call<SignUpModel>, response: Response<SignUpModel>) {
                Log.e(TAG, response.body().toString())
                dismissProgressDialog()
                var  mSignUpModel = response.body()
                if (mSignUpModel!!.status == 1) {
                 //   showToast(mActivity, mSignUpModel.message)
                    showOpenActivityAlertDialog(mActivity, getString(R.string.you_have_sucessfully_signed_up))
                    nameET.setText("")
                    editEmailET.setText("")
                    passwordET.setText("")
                    confirmPasswordET.setText("")
//                    val i = Intent(mActivity, SignInActivity::class.java)
//                    startActivity(i)
//                    finish()
                }else if (mSignUpModel.status == 0) {
                    showAlertDialog(mActivity, mSignUpModel.message)
                } else {
                    showAlertDialog(mActivity, getString(R.string.you_have_not_verified_your_email_address_please_check_your_email))
                }
            }
        })
    }

    private fun clickOnshowHidePwd() {
        if(passwordET.getTransformationMethod().equals(PasswordTransformationMethod.getInstance())){
            password_toggle_btn.background=getDrawable(R.drawable.ic_show_pwd)

            //Show Password
            passwordET.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
        }
        else{
            password_toggle_btn.background=getDrawable(R.drawable.ic_hide_pwd)

            //Hide Password
            passwordET.setTransformationMethod(PasswordTransformationMethod.getInstance());

        }
    }
    private fun performAgreeClick() {
        if (checkUncheck == 0) {
            imgAgreeCV.setBackgroundResource(R.drawable.ic_check)
            checkUncheck++
        } else {
            imgAgreeCV.setBackgroundResource(R.drawable.ic_uncheck)
            checkUncheck--
        }
    }
    // - - Validations for Sign Up fields
    private fun isValidate(): Boolean {
        var flag = true
        when {
            nameET.text.toString().trim { it <= ' ' } == "" -> {
                showAlertDialog(mActivity, getString(R.string.please_enter_name))
                flag = false
            }

            editEmailET.text.toString().trim { it <= ' ' } == "" -> {
                showAlertDialog(mActivity, getString(R.string.please_enter_email))
                flag = false
            }
            !isValidEmaillId(editEmailET.text.toString().trim { it <= ' ' }) -> {
                showAlertDialog(mActivity, getString(R.string.please_enter_valid_email_address))
                flag = false
            }
            passwordET.text.toString().trim { it <= ' ' } == "" -> {
                showAlertDialog(mActivity, getString(R.string.please_enter_password))
                flag = false
            }
            passwordET.text.toString().length < 6 -> {
                showAlertDialog(mActivity, getString(R.string.please_enter_minimum_6_))
                flag = false
            }
            confirmPasswordET.text.toString().trim { it <= ' ' } == "" -> {
                showAlertDialog(mActivity, getString(R.string.please_enter_confirm_password))
                flag = false
            }
            confirmPasswordET.text.toString().length < 6 -> {
                showAlertDialog(mActivity, getString(R.string.please_enter_minimum_6_))
                flag = false
            }
            confirmPasswordET.text.toString().trim { it <= ' ' } != passwordET.text.toString() -> {
                showAlertDialog(mActivity, getString(R.string.please_enter_same_new_password_and_confirm_password))
                flag = false
            }
            checkUncheck == 0 -> {
                showAlertDialog(mActivity, getString(R.string.please_agree_terms_and_conditions))
                flag = false
            }

        }
        return flag
    }
}
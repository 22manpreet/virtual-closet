package com.virtualcloset.app.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.ImageView
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.virtualcloset.app.R
import com.virtualcloset.app.utils.TERMS_AND_CONDITIONS

class TermsAndConditionsActivity : BaseActivity() {
    @BindView(R.id.backRL)
    lateinit var backRL: ImageView

    @BindView(R.id.webView)
    lateinit var webView: WebView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_terms_and_conditions)
        ButterKnife.bind(mActivity)
        webView.webViewClient = WebViewClient()

        // this will load the url of the website
        webView.loadUrl(TERMS_AND_CONDITIONS)

        // this will enable the javascript settings
        webView.settings.javaScriptEnabled = true

        // if you want to enable zoom feature
        webView.settings.setSupportZoom(true)
    }
    @OnClick(
        R.id.backRL
    )
    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.backRL -> onBackPressed()

        }
    }

    override fun onBackPressed() {
        // if your webview can go back it will go back
        if (webView.canGoBack())
            webView.goBack()
        // if your webview cannot go back
        // it will exit the application
        else
            super.onBackPressed()
    }
}
package com.virtualcloset.app.activity

import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.TextView
import android.widget.Toast
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.android.billingclient.api.*
import com.android.billingclient.api.BillingClient.BillingResponseCode
import com.android.billingclient.api.BillingClient.SkuType.INAPP
import com.virtualcloset.app.R
import java.io.IOException
import com.android.billingclient.api.Purchase.PurchaseState
import com.virtualcloset.app.Model.AddSubscriptionModel
import com.virtualcloset.app.Model.SubscriptionDetailModel
import com.virtualcloset.app.Retrofit.RetrofitClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


class FreeTrialActivity : BaseActivity()  //, PurchasesUpdatedListener
{
//    @BindView(R.id.purchase_status)
//    lateinit var purchaseStatus : TextView

    private var billingClient: BillingClient? = null

    @BindView(R.id.purchase_button)
    lateinit var purchase_button: TextView

    var subscriptionDetail: ArrayList<SubscriptionDetailModel?>? = ArrayList()
    var purchaseTime: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_free_trial)
        ButterKnife.bind(mActivity)

//        billingClient = BillingClient.newBuilder(this)
//            .enablePendingPurchases().setListener(this).build()
//        billingClient!!.startConnection(object : BillingClientStateListener {
//            override fun onBillingSetupFinished(billingResult: BillingResult) {
//                if (billingResult.responseCode == BillingResponseCode.OK) {
//                    val queryPurchase = billingClient!!.queryPurchases(INAPP)
//                    val queryPurchases: List<Purchase>? = queryPurchase.purchasesList
//                    for(i in 0..queryPurchases!!.size) {
//                       purchaseTime= queryPurchases!!.get(i).purchaseTime.toString()
//                      //  queryPurchases!!.get(i).purchaseTime.also { purchaseTime = it }
//                    }
//
//                    if (queryPurchases != null && queryPurchases.size > 0) {
//                        handlePurchases(queryPurchases)
//                    }
//                    //if purchase list is empty that means item is not purchased
//                    //Or purchase is refunded or canceled
//                    else{
//                        savePurchaseValueToPref(false)
//                    }
//                }
//            }
//
//            override fun onBillingServiceDisconnected() {}
//        })
//
//        //item Purchased
//        if (purchaseValueFromPref) {
//            purchase_button!!.visibility = View.GONE
//            showToast(mActivity,"Purchase Status : Purchased")
//
//            executeSubscriptionDataRequest(purchaseTime)
//
////            val i = Intent(mActivity, HomeActivity::class.java)
////            startActivity(i)
//        }
//        //item not Purchased
//        else {
//            purchase_button!!.visibility = View.VISIBLE
//            showToast(mActivity,"Purchase Status : Not Purchased")
//        }
    }
    private fun mParam(): MutableMap<String?, Any?> {
        val mMap: MutableMap<String?, Any?> = HashMap()
        mMap["plan_id"] = "2"
        mMap["subscription_details"]= subscriptionDetail
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }
    private fun executeSubscriptionDataRequest() {
     //   var purchase_Time =SimpleDateFormat("yyyy-MM-dd HH:mm:ss zzzz yyyy").format(Date(purchaseTime!!.toLong())).toString()

        val headers: MutableMap<String, String> = HashMap()
        headers["Token"] = getAuthToken()

        subscriptionDetail!!.add(SubscriptionDetailModel("","","", ""))

        showProgressDialog(mActivity)
        val call = RetrofitClient.apiInterface.AddSubscriptionDataRequest(headers,mParam())

        call.enqueue(object : Callback<AddSubscriptionModel> {
            override fun onFailure(call: Call<AddSubscriptionModel>, t: Throwable) {
                Log.e(TAG, t.message.toString())
                dismissProgressDialog()
            }
            override fun onResponse(call: Call<AddSubscriptionModel>, response: Response<AddSubscriptionModel>) {
                Log.e(TAG, response.body().toString())
                dismissProgressDialog()
                var  mAddSubscriptionModel = response.body()
                if (mAddSubscriptionModel!!.status == 1) {
                    val i = Intent(mActivity, HomeActivity::class.java)
                    startActivity(i)
                }
                else {
                     showAlertDialog(mActivity,mAddSubscriptionModel!!.message)
                }
            }
        })
    }

    @OnClick(
        R.id.purchase_button
    )
    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.purchase_button -> performStartClick()

        }
    }

    private fun performStartClick() {
        if (isNetworkAvailable(mActivity!!)) {
            executeSubscriptionDataRequest()
        } else {
            showToast(mActivity, getString(R.string.internet_connection_error))
        }
//        val i = Intent(mActivity, HomeActivity::class.java)
//        startActivity(i)
        //check if service is already connected

//        if (billingClient!!.isReady) {
//            initiatePurchase()
//        }
//        //else reconnect service
//        else {
//            billingClient = BillingClient.newBuilder(this).enablePendingPurchases().setListener(this).build()
//            billingClient!!.startConnection(object : BillingClientStateListener {
//                override fun onBillingSetupFinished(billingResult: BillingResult) {
//                    if (billingResult.responseCode == BillingResponseCode.OK) {
//                        initiatePurchase()
//                    } else {
//                        Toast.makeText(applicationContext, "Error " + billingResult.debugMessage, Toast.LENGTH_SHORT).show()
//                    }
//                }
//
//                override fun onBillingServiceDisconnected() {}
//            })
//        }
    }


//    private val preferenceObject: SharedPreferences
//        get() = applicationContext.getSharedPreferences(PREF_FILE, 0)
//
//    private val preferenceEditObject: SharedPreferences.Editor
//        get() {
//            val pref: SharedPreferences = applicationContext.getSharedPreferences(PREF_FILE, 0)
//            return pref.edit()
//        }
//
//    private val purchaseValueFromPref: Boolean
//        get() = preferenceObject.getBoolean(PURCHASE_KEY, false)
//
//    private fun savePurchaseValueToPref(value: Boolean) {
//        preferenceEditObject.putBoolean(PURCHASE_KEY, value).commit()
//    }
//
//    private fun initiatePurchase() {
//
//        val skuList: MutableList<String> = ArrayList()
//        skuList.add(PRODUCT_ID)
//        val params = SkuDetailsParams.newBuilder()
//        params.setSkusList(skuList).setType(INAPP)
//
//      billingClient!!.querySkuDetailsAsync(params.build()) { billingResult, skuDetailsList ->
//            if (billingResult.responseCode == BillingResponseCode.OK) {
//                if (skuDetailsList != null && skuDetailsList.size > 0) {
//                    val flowParams = BillingFlowParams.newBuilder()
//                        .setSkuDetails(skuDetailsList[0])
//                        .build()
//                    billingClient!!.launchBillingFlow(mActivity, flowParams)
//                } else {
//                    //try to add item/product id "purchase" inside managed product in google play console
//
//                    Toast.makeText(applicationContext, "Purchase Item not Found", Toast.LENGTH_SHORT).show()
//                }
//            } else {
//                Toast.makeText(applicationContext,
//                    " Error " + billingResult.debugMessage, Toast.LENGTH_SHORT).show()
//            }
//        }
//    }

//    override fun onPurchasesUpdated(billingResult: BillingResult, purchases: List<Purchase>?) {
//        //if item newly purchased
//
//        if (billingResult.responseCode == BillingResponseCode.OK && purchases != null) {
//            handlePurchases(purchases)
//        }
//        //if item already purchased then check and reflect changes
//        else if (billingResult.responseCode == BillingResponseCode.ITEM_ALREADY_OWNED) {
//            val queryAlreadyPurchasesResult = billingClient!!.queryPurchases(INAPP)
//            val alreadyPurchases: List<Purchase>? = queryAlreadyPurchasesResult.purchasesList
//            if (alreadyPurchases != null) {
//                handlePurchases(alreadyPurchases)
//            }
//        }
//        //if purchase cancelled
//        else if (billingResult.responseCode == BillingResponseCode.USER_CANCELED) {
//            Toast.makeText(applicationContext, "Purchase Canceled", Toast.LENGTH_SHORT).show()
//        }
//        // Handle any other error msgs
//        else {
//            Toast.makeText(applicationContext, "Error " + billingResult.debugMessage, Toast.LENGTH_SHORT).show()
//        }
//    }

//    fun handlePurchases(purchases: List<Purchase>) {
//        for (purchase in purchases) {
//            //if item is purchased
//
//            if (PRODUCT_ID == purchase.skus.toString() && purchase.purchaseState == PurchaseState.PURCHASED) {
//                if (!verifyValidSignature(purchase.originalJson, purchase.signature)) {
//                    // Invalid purchase
//                    // show error to user
//
//                    Toast.makeText(applicationContext, "Error : Invalid Purchase", Toast.LENGTH_SHORT).show()
//                    return
//                }
//                // else purchase is valid
//                //if item is purchased and not acknowledged
//
//
//                if (!purchase.isAcknowledged) {
//                    val acknowledgePurchaseParams = AcknowledgePurchaseParams.newBuilder()
//                        .setPurchaseToken(purchase.purchaseToken)
//                        .build()
//                    billingClient!!.acknowledgePurchase(acknowledgePurchaseParams, ackPurchase)
//                }
//                //else item is purchased and also acknowledged
//                else {
//                    // Grant entitlement to the user on item purchase
//                    // restart activity
//
//                    if (!purchaseValueFromPref) {
//                        savePurchaseValueToPref(true)
//                        Toast.makeText(applicationContext, "Item Purchased", Toast.LENGTH_SHORT).show()
//                        recreate()
//                        purchaseTime=purchase.purchaseTime.toString()
//                        executeSubscriptionDataRequest(purchaseTime)
////                        val i = Intent(mActivity, HomeActivity::class.java)
////                        startActivity(i)
//                    }
//                }
//            }
//            //if purchase is pending
//            else if (PRODUCT_ID == purchase.skus.toString() && purchase.purchaseState == PurchaseState.PENDING) {
//                Toast.makeText(applicationContext,
//                    "Purchase is Pending. Please complete Transaction", Toast.LENGTH_SHORT).show()
//            }
//            //if purchase is refunded or unknown
//            else if (PRODUCT_ID == purchase.skus.toString() && purchase.purchaseState == PurchaseState.UNSPECIFIED_STATE) {
//                savePurchaseValueToPref(false)
//               showToast(mActivity,"Purchase Status : Not Purchased")
//                purchase_button!!.visibility = View.VISIBLE
//                Toast.makeText(applicationContext, "Purchase Status Unknown", Toast.LENGTH_SHORT).show()
//            }
//        }
//    }
//
//    var ackPurchase = AcknowledgePurchaseResponseListener { billingResult ->
//        if (billingResult.responseCode == BillingResponseCode.OK) {
//            //if purchase is acknowledged
//            // Grant entitlement to the user. and restart activity
//
//            savePurchaseValueToPref(true)
//            Toast.makeText(applicationContext, "Item Purchased", Toast.LENGTH_SHORT).show()
//            recreate()
//
//            executeSubscriptionDataRequest(purchaseTime)
////            val i = Intent(mActivity, HomeActivity::class.java)
////            startActivity(i)
//        }
//    }
//    /**
//     * Verifies that the purchase was signed correctly for this developer's public key.
//     *
//     * Note: It's strongly recommended to perform such check on your backend since hackers can
//     * replace this method with "constant true" if they decompile/rebuild your app.
//     *
//     */
//    private fun verifyValidSignature(signedData: String, signature: String): Boolean {
//        return try {
//            // To get key go to Developer Console > Select your app > Development Tools > Services & APIs.
//
//            val base64Key = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAiM9mscXA0Rp52P98sMq2DqKz3sokpFjRK9fT/GxAlieGMhdEWsl4mq731O9DwU2fXDnsdtrmbY7gq36a7Og9SII4v6C2wAg99rC1kGpminje7JAfprDWP7prkomVkiaONb9OhUR/nDtj9/GSWJdNaL0nQmge9XYl2kmXI/yMq59NfLCsp9Eeaoiq9aUVSnyZz9wVPT02qirsuzW8WWRmOVAe3QcvynNFp9bLCCS5LrW+MhETiqF33nWfahdDtNxYGiQYvQCs/mtHQcXgmfASBC9TAQDbmY6UeKb2LeslmNiWtU+pbPgOI0ZGNG7evFwxmS5I+NerCvP4n7DD7taImwIDAQAB"
//            Security.verifyPurchase(base64Key, signedData, signature)
//        } catch (e: IOException) {
//            false
//        }
//    }
//
//    override fun onDestroy() {
//        super.onDestroy()
//        if (billingClient != null) {
//            billingClient!!.endConnection()
//        }
//    }
//
//    companion object {
//        const val PREF_FILE = "MyPref"
//        const val PURCHASE_KEY = "purchase item"
//        const val PRODUCT_ID = "one_time_in_app_product_123"
//    }
}
package com.virtualcloset.app.activity

import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.Window
import android.widget.ImageView
import android.widget.TextView
import butterknife.ButterKnife

import androidx.viewpager.widget.ViewPager
import butterknife.BindView
import com.google.android.material.tabs.TabLayout
import com.virtualcloset.app.R
import com.virtualcloset.app.adapter.MyCustomPagerAdapter
import butterknife.OnClick
import com.virtualcloset.app.Model.*
import com.virtualcloset.app.Retrofit.RetrofitClient
import io.branch.indexing.BranchUniversalObject
import io.branch.referral.Branch
import io.branch.referral.util.ContentMetadata
import io.branch.referral.util.LinkProperties
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.HashMap

class DetailActivity : BaseActivity() {
    @BindView(R.id.viewPager)
    lateinit var viewPager: ViewPager

    @BindView(R.id.tab_layout)
    lateinit var tab_layout: TabLayout

    @BindView(R.id.backRL)
    lateinit var backRL: ImageView

    @BindView(R.id.deleteIV)
    lateinit var deleteIV: ImageView

    @BindView(R.id.nameTV)
    lateinit var nameTV: TextView

    @BindView(R.id.descriptionTV)
    lateinit var descriptionTV: TextView

    @BindView(R.id.isFavTV)
    lateinit var isFavTV: TextView

    @BindView(R.id.shareTV)
    lateinit var shareTV: TextView
   // var images = intArrayOf(R.drawable.shoe1, R.drawable.shoe2, R.drawable.shoe3, R.drawable.shoe4,R.drawable.shoe5,R.drawable.shoes)
   var images:ArrayList<String>?=ArrayList()
    var myCustomPagerAdapter: MyCustomPagerAdapter? = null
    var itemDetailsArrayList: ArrayList<ItemDetailsItem?>?=null
    var categoryName:String?=null
    var item_id:String?=null
    var category_id:String?=null
    var description:String?=null
    var image1:String?=null
    var image2:String?=null
    var image3:String?=null
    var image4:String?=null
    var image5:String?=null
    var is_fav:String?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)
        //Add below line to hide status bar
        ButterKnife.bind(mActivity)
        item_id=intent.getStringExtra("item_id")
            if (isNetworkAvailable(mActivity)) {
                executeGetDetailRequest()
            } else {
                showToast(mActivity, getString(R.string.internet_connection_error))
            }
    }

    private fun executeGetDetailRequest() {
        val headers: MutableMap<String, String> = HashMap()
        headers["Token"] = getAuthToken()

        val item_id: RequestBody = item_id.toString().toRequestBody("multipart/form-data".toMediaTypeOrNull())
        showProgressDialog(mActivity)
        val call = RetrofitClient.apiInterface. GetItemsDetailsByIdDataRequest(headers,item_id)

        call.enqueue(object : Callback<GetItemDetailedByIdModel> {
            override fun onFailure(call: Call<GetItemDetailedByIdModel>, t: Throwable) {
                Log.e(TAG, t.message.toString())
                dismissProgressDialog()
            }
            override fun onResponse(call: Call<GetItemDetailedByIdModel>, response: Response<GetItemDetailedByIdModel>) {
                Log.e(TAG, response.body().toString())
                dismissProgressDialog()
                var  mGetItemDetailedByIdModel = response.body()
                if (mGetItemDetailedByIdModel!!.status == 1) {
                    categoryName=mGetItemDetailedByIdModel.data!!.categoryName
                    descriptionTV.setText(mGetItemDetailedByIdModel.data!!.description)
                    image1=(mGetItemDetailedByIdModel.data!!.image1)
                    image2=mGetItemDetailedByIdModel.data!!.image2
                    image3=mGetItemDetailedByIdModel.data!!.image3
                    image4=mGetItemDetailedByIdModel.data!!.image4
                    image5=mGetItemDetailedByIdModel.data!!.image5
                    is_fav=mGetItemDetailedByIdModel.data!!.isFav
                    addValues()
                }
                else {
                    showAlertDialog(mActivity, mGetItemDetailedByIdModel.message)
                }
            }
        })
    }

    private fun addValues() {
        if(image1!!.length > 43) {
            images!!.add(image1!!)
        }
        if(image2!!.length > 43) {
            images!!.add(image2!!)
        }
        if(image3!!.length > 43) {
            images!!.add(image3!!)
        }
        if(image4!!.length > 43) {
            images!!.add(image4!!)
        }
        if(image5!!.length > 43) {
            images!!.add(image5!!)
        }
        myCustomPagerAdapter = MyCustomPagerAdapter(mActivity, images!!)
        viewPager.adapter = myCustomPagerAdapter

        tab_layout.setupWithViewPager(viewPager, true)
        if(categoryName.equals("")) {

        }
        else{
            nameTV.setText(categoryName!!.substring(0, 1).toUpperCase() + categoryName!!.substring(1).toLowerCase()
            )
        }


        if(is_fav.equals("2")){
            isFavTV.setBackgroundResource(R.drawable.ic_unfav)
        }
        else if(is_fav.equals("1")){
            isFavTV.setBackgroundResource(R.drawable.ic_fav)
        }
    }

    @OnClick(
        R.id.backRL,R.id.deleteIV,R.id.isFavTV,R.id.shareTV
    )
    fun onViewClicked(view: View){
        when(view.id) {
            R.id.backRL -> onBackPressed()
            R.id.deleteIV -> performDeleteClick()
            R.id.isFavTV -> performOnFavClick()
            R.id.shareTV -> performShareClick()
        }
    }

    private fun performShareClick() {
        performShareClick(item_id)
    }


    private fun performShareClick(itemId: String?) {

        val buo = BranchUniversalObject()
            .setCanonicalIdentifier("content/12345")
            .setTitle("Virtual Closet")
        //    .setContentImageUrl("https://forgivityapp.com/v2/uploads/ic_splash.png")
            .setContentIndexingMode(BranchUniversalObject.CONTENT_INDEX_MODE.PUBLIC)
            .setLocalIndexMode(BranchUniversalObject.CONTENT_INDEX_MODE.PUBLIC)
            .setContentMetadata(ContentMetadata().addCustomMetadata("item_id", itemId))

        val lp = LinkProperties()
            .setFeature("sharing")
            .setCampaign("content_sharing")
            .setStage("new user")

        buo.generateShortUrl(
            mActivity, lp,
            Branch.BranchLinkCreateListener { url, error ->
                if (error == null) {
// scanGallery(context, pictureFile.absolutePath, downUri.toString())
                    shareData(url)
                    Log.e("Checki", "3")
                } else {
                    var url1 = ""
                    Log.e("Checki", "4")
                }
            })
    }

    private fun shareData(url: String?) {
        val intent = Intent()
        intent.action = Intent.ACTION_SEND
        intent.type = "text/plain"
        intent.putExtra(Intent.EXTRA_TEXT, url)
        startActivity(Intent.createChooser(intent, "Share with:"))
    }

    private fun performOnFavClick() {
        if(is_fav.equals("2")){
            is_fav="1"
        }
        else if(is_fav.equals("1")){
            is_fav="2"
        }
        executeAddEditFavItemsRequest()
    }
    private fun executeAddEditFavItemsRequest() {
        val headers: MutableMap<String, String> = HashMap()
        headers["Token"] = getAuthToken()

        val item_id: RequestBody = item_id!!.trim().toRequestBody("multipart/form-data".toMediaTypeOrNull())
        val favourite : RequestBody = is_fav!!.toString().trim().toRequestBody("multipart/form-data".toMediaTypeOrNull())

        showProgressDialog(mActivity)
        val call = RetrofitClient.apiInterface.addEditFavItemsDataRequest(headers,item_id,favourite)

        call.enqueue(object : Callback<AddEditFavItemsModel> {
            override fun onFailure(call: Call<AddEditFavItemsModel>, t: Throwable) {
                Log.e(TAG, t.message.toString())
                dismissProgressDialog()
            }
            override fun onResponse(call: Call<AddEditFavItemsModel>, response: Response<AddEditFavItemsModel>) {
                Log.e(TAG, response.body().toString())
                dismissProgressDialog()
                var mAddEditFavItemsModel = response.body()!!
                if (mAddEditFavItemsModel!!.status == 1) {
                    if(mAddEditFavItemsModel.message.equals("Item Add in favlist.")){
                        isFavTV.setBackgroundResource(R.drawable.ic_fav)
                    //    is_fav="1"
                    }
                    else if(mAddEditFavItemsModel.message.equals("Added in favorite list.")){
                        isFavTV.setBackgroundResource(R.drawable.ic_fav)
                 //       is_fav="1"
                    }
//                       if(is_fav.equals("1")){
//                           isFavTV.setBackgroundResource(R.drawable.ic_fav)
////
//                       }
                    else {
                        isFavTV.setBackgroundResource(R.drawable.ic_unfav)
                      //  is_fav="2"
                    }
                }
                else if(mAddEditFavItemsModel!!.status == 0){
                  //  showToast(mActivity,mAddEditFavItemsModel!!.message)
                    if(mAddEditFavItemsModel.message.equals("Unautorized request, please login again.")){
                        var intent= Intent(mActivity,SignInActivity::class.java)
                        startActivity(intent)
                    }
                }
                else {
                    showAlertDialog(mActivity, getString(R.string.internal_server_error))
                }
            }
        })
    }
    private fun performDeleteClick() {
    showDeleteAlertDialog(mActivity,getString(R.string.are_you_sure_you_want_to_delete))
    }

    /*
*
*  Alert Dialog
* */
    fun showDeleteAlertDialog(mActivity: Activity?, strMessage: String?) {
        val alertDialog = Dialog(mActivity!!)
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        alertDialog.setContentView(R.layout.delete_dialog_alert)
        alertDialog.setCanceledOnTouchOutside(false)
        alertDialog.setCancelable(false)
        alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        // set the custom dialog components - text, image and button
        val txtMessageTV = alertDialog.findViewById<TextView>(R.id.txtMessageTV)
        val noTV = alertDialog.findViewById<TextView>(R.id.noTV)
        val yesTV = alertDialog.findViewById<TextView>(R.id.yesTV)
        txtMessageTV.text = strMessage
        noTV.setOnClickListener { alertDialog.dismiss() }
        yesTV.setOnClickListener { alertDialog.dismiss()
            if (isNetworkAvailable(mActivity)) {
                executeDeleteItemRequest()
            }
            else{
                showToast(mActivity, getString(R.string.internet_connection_error))
            }
        }
        alertDialog.show()
    }

    private fun executeDeleteItemRequest() {
        val headers: MutableMap<String, String> = HashMap()
        headers["Token"] = getAuthToken()

        val item_id: RequestBody = item_id!!.toRequestBody("multipart/form-data".toMediaTypeOrNull())

        showProgressDialog(mActivity)
        val call = RetrofitClient.apiInterface.DeleteItemDataRequest(headers,item_id)

        call.enqueue(object : Callback<DeleteItemModel> {
            override fun onFailure(call: Call<DeleteItemModel>, t: Throwable) {
                Log.e(TAG, t.message.toString())
                dismissProgressDialog()
            }
            override fun onResponse(call: Call<DeleteItemModel>, response: Response<DeleteItemModel>) {
                Log.e(TAG, response.body().toString())
                dismissProgressDialog()
               var mDeleteItemModel = response.body()!!
                if (mDeleteItemModel!!.status == 1) {
                 onBackPressed()
                }
                else if(mDeleteItemModel!!.status == 0){
                    showToast(mActivity,mDeleteItemModel!!.message)
                }
                else {
                    showAlertDialog(mActivity, getString(R.string.internal_server_error))
                }
            }
        })
    }

    override fun onBackPressed() {
        super.onBackPressed()
    }
}
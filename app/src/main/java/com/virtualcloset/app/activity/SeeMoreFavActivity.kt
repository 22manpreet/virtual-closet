package com.virtualcloset.app.activity

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.GridView
import android.widget.ImageView
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.virtualcloset.app.Model.FavDetailDataItem

import com.virtualcloset.app.Model.GetallitemfavdetailsbycategoryModel
import com.virtualcloset.app.R
import com.virtualcloset.app.Retrofit.RetrofitClient
import com.virtualcloset.app.adapter.SeeMoreFavAdapter
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.HashMap

class SeeMoreFavActivity : BaseActivity() {
    @BindView(R.id.backRL)
    lateinit var backRL: ImageView

    @BindView(R.id.nameTV)
    lateinit var nameTV: TextView

    @BindView(R.id.gridView)
    lateinit var gridView: GridView

    var seeMoreFavAdapter: SeeMoreFavAdapter? = null

    var favDetailsItemArrayList: ArrayList<FavDetailDataItem?>? = ArrayList()

    var catgoryName:String?=null
    var category_id : String?=null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_see_more_fav)
        ButterKnife.bind(mActivity)

        category_id =intent.getStringExtra("category_id")
        catgoryName= intent.getStringExtra("category_name")
        nameTV.setText(catgoryName!!.substring(0, 1).toUpperCase() + catgoryName!!.substring(1).toLowerCase())
        if (isNetworkAvailable(mActivity!!)) {
            executeGetallitemfavdetailsbycategoryDataRequest()
        } else {
            showToast(mActivity, getString(R.string.internet_connection_error))
        }
    }
    private fun executeGetallitemfavdetailsbycategoryDataRequest() {
        val headers: MutableMap<String, String> = HashMap()
        headers["Token"] = getAuthToken()

        val category_id: RequestBody = category_id!!.toRequestBody("multipart/form-data".toMediaTypeOrNull())
        val pageNo: RequestBody = "1".toRequestBody("multipart/form-data".toMediaTypeOrNull())
        val per_page: RequestBody = "10".toRequestBody("multipart/form-data".toMediaTypeOrNull())

        showProgressDialog(mActivity)
        val call = RetrofitClient.apiInterface.GetallitemfavdetailsbycategoryDataRequest(
            headers,
            category_id,pageNo,per_page)

        call.enqueue(object : Callback<GetallitemfavdetailsbycategoryModel> {
            override fun onFailure(call: Call<GetallitemfavdetailsbycategoryModel>, t: Throwable) {
                Log.e(TAG, t.message.toString())
                dismissProgressDialog()
            }
            override fun onResponse(call: Call<GetallitemfavdetailsbycategoryModel>, response: Response<GetallitemfavdetailsbycategoryModel>) {
                Log.e(TAG, response.body().toString())
                dismissProgressDialog()
                var  getallitemfavdetailsbycategoryModel = response.body()
                if (getallitemfavdetailsbycategoryModel!!.status == 1) {
                    favDetailsItemArrayList=getallitemfavdetailsbycategoryModel.data
                    setAdapter()
                }
                else if(getallitemfavdetailsbycategoryModel!!.status == 0){
                    if(getallitemfavdetailsbycategoryModel.message.equals("Unautorized request, please login again.")){
                        var intent=Intent(mActivity,SignInActivity::class.java)
                        startActivity(intent)
                    }
                }
                else {
                    showAlertDialog(mActivity, getString(R.string.internal_server_error))
                }
            }
        })
    }

    @OnClick(
        R.id.backRL
    )
    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.backRL -> onBackPressed()
        }
    }
    private fun setAdapter() {
        seeMoreFavAdapter = SeeMoreFavAdapter(mActivity,favDetailsItemArrayList)
        gridView.adapter = seeMoreFavAdapter

        gridView.onItemClickListener = AdapterView.OnItemClickListener { _, _, position, _ ->
            val intent = Intent(mActivity, DetailActivity::class.java)
            intent.putExtra("category_name",catgoryName)
            intent.putExtra("item_id",favDetailsItemArrayList!!.get(position)!!.itemId)
//            intent.putExtra("category_id",favDetailsItemArrayList!!.get(position)!!.categoryId)
//            intent.putExtra("description",favDetailsItemArrayList!!.get(position)!!.description)
//            intent.putExtra("image1",favDetailsItemArrayList!!.get(position)!!.image1)
//            intent.putExtra("image2",favDetailsItemArrayList!!.get(position)!!.image2)
//            intent.putExtra("image3",favDetailsItemArrayList!!.get(position)!!.image3)
//            intent.putExtra("image4",favDetailsItemArrayList!!.get(position)!!.image4)
//            intent.putExtra("image5",favDetailsItemArrayList!!.get(position)!!.image5)
//            intent.putExtra("is_fav",favDetailsItemArrayList!!.get(position)!!.isFav)
            startActivity(intent)
        }
    }
}
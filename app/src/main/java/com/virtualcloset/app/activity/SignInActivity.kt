package com.virtualcloset.app.activity

import android.content.Intent
import android.os.Bundle
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.util.Log
import android.view.View
import android.widget.CheckBox
import android.widget.EditText
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.virtualcloset.app.Model.ForgotPasswordModel
import com.virtualcloset.app.Model.LoginModel
import com.virtualcloset.app.Model.SignUpModel
import com.virtualcloset.app.Model.SubscriptionStatusModel
import com.virtualcloset.app.R
import com.virtualcloset.app.Retrofit.RetrofitClient
import com.virtualcloset.app.utils.*
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.HashMap

class SignInActivity : BaseActivity() {
    @BindView(R.id.txtSignUpTV)
    lateinit var txtSignUpTV: TextView

    @BindView(R.id.txtLogInTV)
    lateinit var txtLogInTV: TextView

    @BindView(R.id.txtForgotPasswordTV)
    lateinit var txtForgotPasswordTV: TextView

    @BindView(R.id.editEmailET)
    lateinit var editEmailET: EditText

    @BindView(R.id.editPasswordET)
    lateinit var editPasswordET: EditText

    @BindView(R.id.toggle_btn)
    lateinit var toggle_btn:TextView

   @BindView(R.id.imgAgreeIV)
   lateinit var imgAgreeCB: CheckBox

    private var checkUncheck: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_in)
        ButterKnife.bind(mActivity)
    }

    @OnClick(
        R.id.txtSignUpTV,
        R.id.txtLogInTV,
        R.id.txtForgotPasswordTV,
        R.id.toggle_btn,
        R.id.imgAgreeIV
    )
    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.txtSignUpTV -> performSignUpClick()
            R.id.txtLogInTV -> performLogInClick()
            R.id.txtForgotPasswordTV -> performForgetPasswordClick()
            R.id.toggle_btn -> clickOnshowHidePwd()
            R.id.imgAgreeIV -> performAgreeClick()
        }
    }

    private fun performAgreeClick() {
        if (checkUncheck == 0) {
            imgAgreeCB.setBackgroundResource(R.drawable.ic_check)
            checkUncheck++
        } else {
            imgAgreeCB.setBackgroundResource(R.drawable.ic_uncheck)
            checkUncheck--
        }
    }

    private fun clickOnshowHidePwd() {
        if(editPasswordET.getTransformationMethod().equals(PasswordTransformationMethod.getInstance())){
            toggle_btn.background=getDrawable(R.drawable.ic_show_pwd)

            //Show Password
            editPasswordET.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
        }
        else{
            toggle_btn.background=getDrawable(R.drawable.ic_hide_pwd)

            //Hide Password
            editPasswordET.setTransformationMethod(PasswordTransformationMethod.getInstance());

        }
    }
    // - - Validations for Sign In fields
    private fun isValidate(): Boolean {
        var flag = true
        when {
            editEmailET.text.toString().trim { it <= ' ' } == "" -> {
                showAlertDialog(mActivity, getString(R.string.please_enter_email))
                flag = false
            }
            !isValidEmaillId(editEmailET.text.toString().trim { it <= ' ' }) -> {
                showAlertDialog(mActivity, getString(R.string.please_enter_valid_email_address))
                flag = false
            }
            editPasswordET.text.toString().trim { it <= ' ' } == "" -> {
                showAlertDialog(mActivity, getString(R.string.please_enter_password))
                flag = false
            }
        }
        return flag
    }
    private fun performForgetPasswordClick() {
        val i = Intent(mActivity, ForgetPasswordActivity::class.java)
        startActivity(i)
    }

    private fun performLogInClick() {
        if(isValidate()) {
            if (isNetworkAvailable(mActivity)) {
                executeSignInRequest()
            } else {
                showToast(mActivity, getString(R.string.internet_connection_error))
            }
        }
    }

    private fun executeSignInRequest() {

        val email: RequestBody = editEmailET.text.toString().trim()
            .toRequestBody("multipart/form-data".toMediaTypeOrNull())
        val password: RequestBody =  editPasswordET.text.toString().trim()
            .toRequestBody("multipart/form-data".toMediaTypeOrNull())
        showProgressDialog(mActivity)
        val call = RetrofitClient.apiInterface.LogInDataRequest(
            email,
            password,
        )

        call.enqueue(object : Callback<LoginModel> {
            override fun onFailure(call: Call<LoginModel>, t: Throwable) {
                Log.e(TAG, t.message.toString())
                dismissProgressDialog()
            }
            override fun onResponse(call: Call<LoginModel>, response: Response<LoginModel>) {
                Log.e(TAG, response.body().toString())
                dismissProgressDialog()
                var  mLoginModel = response.body()
                if (mLoginModel!!.status == 1) {
                    showToast(mActivity, mLoginModel.message)
                    AppPrefrences().writeBoolean(mActivity, IS_LOGIN, true)
                    AppPrefrences().writeString(mActivity, USERID, mLoginModel.data!!.userId)
                    AppPrefrences().writeString(mActivity, EMAIL, mLoginModel.data!!.email)
                    AppPrefrences().writeString(mActivity, NAME, mLoginModel.data!!.name)
                    AppPrefrences().writeString(mActivity, PASSWORD,  editPasswordET.text.toString())
                    AppPrefrences().writeString(mActivity, VERIFICATIONCODE, mLoginModel.data!!.verificationCode)
                    AppPrefrences().writeString(mActivity, EMAILVERIFICATION, mLoginModel.data!!.emailVerification)
                    AppPrefrences().writeString(mActivity, DEVICENAME, mLoginModel.data!!.deviceName)
                    AppPrefrences().writeString(mActivity, DISABLE,mLoginModel.data!!.disable)
                    AppPrefrences().writeString(mActivity, IMAGE, mLoginModel.data!!.image)
                    AppPrefrences().writeString(mActivity, AUTHTOKEN,mLoginModel.data!!.authtoken)

                    executeCheckSubscriptionPlan()


                }else if (mLoginModel.status == 0) {
                    showAlertDialog(mActivity, mLoginModel.message)
                } else {
                    showAlertDialog(mActivity, getString(R.string.you_have_not_verified_your_email_address_please_check_your_email))
                }
            }
        })
    }

    private fun executeCheckSubscriptionPlan() {
        val headers: MutableMap<String, String> = HashMap()
        headers["Token"] = getAuthToken()
        val call = RetrofitClient.apiInterface.SubscriptionStatusDataRequest(headers)

        call.enqueue(object : Callback<SubscriptionStatusModel> {
            override fun onFailure(call: Call<SubscriptionStatusModel>, t: Throwable) {
                Log.e(TAG, t.message.toString())
                dismissProgressDialog()
            }
            override fun onResponse(call: Call<SubscriptionStatusModel>, response: Response<SubscriptionStatusModel>) {
                Log.e(TAG, response.body().toString())
                dismissProgressDialog()
                var  subscriptionStatusModel = response.body()
                if (subscriptionStatusModel!!.status == 1) {
                    val i = Intent(mActivity, HomeActivity::class.java)
                    startActivity(i)
                    finish()
                }
                else if(subscriptionStatusModel!!.status == 0){
                    val i = Intent(mActivity, FreeTrialActivity::class.java)
                    startActivity(i)
                    finish()
                }
                else if(subscriptionStatusModel!!.status == 7){
                    val i = Intent(mActivity, HomeActivity::class.java)
                    startActivity(i)
                    finish()
                }
                else {
                    showAlertDialog(mActivity, subscriptionStatusModel!!.message)
                }
            }
        })
    }

    private fun performSignUpClick() {
        val i = Intent(mActivity, SignUpActivity::class.java)
        startActivity(i)
    }
}
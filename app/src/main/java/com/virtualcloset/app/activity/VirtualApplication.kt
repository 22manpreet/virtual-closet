package com.virtualcloset.app.activity

import android.app.Application
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.virtualcloset.app.R
import io.branch.referral.Branch

class VirtualApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        // Branch logging for debugging
        Branch.enableLogging()

        // Branch object initialization
        Branch.getAutoInstance(this)
    }
}
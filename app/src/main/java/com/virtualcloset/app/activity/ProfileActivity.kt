package com.virtualcloset.app.activity

import android.annotation.SuppressLint
import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.Window
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.core.app.ActivityCompat
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.bumptech.glide.Glide
import com.virtualcloset.app.Model.GetProfileModel
import com.virtualcloset.app.Model.LogOutModel
import com.virtualcloset.app.Model.SignUpModel
import com.virtualcloset.app.R
import com.virtualcloset.app.Retrofit.RetrofitClient
import com.virtualcloset.app.utils.AppPrefrences
import de.hdodenhof.circleimageview.CircleImageView
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.HashMap

class ProfileActivity : BaseActivity() {

    @BindView(R.id.backRL)
    lateinit var backRL: ImageView

    @BindView(R.id.profileRL)
    lateinit var profileRL: RelativeLayout

    @BindView(R.id.privacyPolicyRL)
    lateinit var privacyPolicyRL: RelativeLayout

    @BindView(R.id.changePwdRL)
    lateinit var changePwdRL: RelativeLayout

    @BindView(R.id.terms_conditionsRL)
    lateinit var terms_conditionsRL: RelativeLayout

    @BindView(R.id.aboutRL)
    lateinit var aboutRL: RelativeLayout

    @BindView(R.id.logoutRL)
    lateinit var logoutRL: RelativeLayout

    @BindView(R.id.profileNameTV)
    lateinit var profileNameTV: TextView

    @BindView(R.id.profileEmailTV)
    lateinit var profileEmailTV: TextView

    @BindView(R.id.profileIV)
    lateinit var profileIV: CircleImageView

    lateinit var  mGetProfileModel:GetProfileModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)
        ButterKnife.bind(mActivity)
    }

    override fun onResume() {
        super.onResume()
        if (isNetworkAvailable(mActivity)) {
            executeGetProfileRequest()
        }
        else{
            showToast(mActivity, getString(R.string.internet_connection_error))
        }
    }
    @OnClick(
        R.id.backRL,R.id.profileRL,R.id.privacyPolicyRL,R.id.changePwdRL,R.id.terms_conditionsRL,R.id.aboutRL,R.id.logoutRL
    )
    fun onViewClicked(view: View){
        when(view.id) {
            R.id.backRL -> onBackPressed()
            R.id.profileRL -> performEditProfileClick()
            R.id.privacyPolicyRL -> performPrivacyPolicyClick()
            R.id.changePwdRL -> performChangePasswordClick()
            R.id.terms_conditionsRL -> performTermsAndConditionsClick()
            R.id.aboutRL -> performAboutClick()
            R.id.logoutRL -> performLogoutClick()
        }
    }
    private fun executeGetProfileRequest() {
        val headers: MutableMap<String, String> = HashMap()
        headers["Token"] = getAuthToken()

        showProgressDialog(mActivity)
        val call = RetrofitClient.apiInterface.GetProfileDataRequest(headers)

        call.enqueue(object : Callback<GetProfileModel> {
            override fun onFailure(call: Call<GetProfileModel>, t: Throwable) {
                Log.e(TAG, t.message.toString())
                dismissProgressDialog()
            }
            override fun onResponse(call: Call<GetProfileModel>, response: Response<GetProfileModel>) {
                Log.e(TAG, response.body().toString())
                dismissProgressDialog()
                 mGetProfileModel = response.body()!!
                if (mGetProfileModel!!.status == 1) {
                    profileNameTV.setText(mGetProfileModel.data!!.name)
                    profileEmailTV.setText(mGetProfileModel.data!!.email)
                    if(mGetProfileModel.data!!.image != ""){
                        Glide.with(mActivity)
                            .load(mGetProfileModel.data!!.image)
                            .placeholder(R.drawable.ic_placeholder)
                            .error(R.drawable.ic_placeholder)
                            .into(profileIV)
                    }
                }
                else if(mGetProfileModel!!.status == 0){
               //     showToast(mActivity,mGetProfileModel!!.message)
                    if(mGetProfileModel.message.equals("Unautorized request, please login again.")){
                        var intent= Intent(mActivity,SignInActivity::class.java)
                        startActivity(intent)
                    }
                }
                else {
                    showAlertDialog(mActivity, getString(R.string.internal_server_error))
                }
            }
        })
    }
    private fun performEditProfileClick() {
        val i = Intent(mActivity, EditProfileActivity::class.java)
        i.putExtra("name",mGetProfileModel.data!!.name)
        i.putExtra("email",mGetProfileModel.data!!.email)
        i.putExtra("image",mGetProfileModel.data!!.image)
        startActivity(i)
    }
    private fun performPrivacyPolicyClick() {
        val i = Intent(mActivity, PrivacyPolicyActivity::class.java)
        startActivity(i)
    }
    private fun performChangePasswordClick() {
        val i = Intent(mActivity, ChangePasswordActivity::class.java)
        startActivity(i)
    }

    private fun performTermsAndConditionsClick() {
        val i = Intent(mActivity, TermsAndConditionsActivity::class.java)
        startActivity(i)
    }
    private fun performAboutClick() {
        val i = Intent(mActivity, AboutActivity::class.java)
        startActivity(i)
    }
    private fun performLogoutClick() {
        showLogoutAlertDialog(mActivity,getString(R.string.are_you_sure_you_want_to_log_out))
    }
    /*
 *
 *  Alert Dialog
 * */
    fun showLogoutAlertDialog(mActivity: Activity?, strMessage: String?) {
        val alertDialog = Dialog(mActivity!!)
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        alertDialog.setContentView(R.layout.delete_dialog_alert)
        alertDialog.setCanceledOnTouchOutside(false)
        alertDialog.setCancelable(false)
        alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        // set the custom dialog components - text, image and button
        val txtMessageTV = alertDialog.findViewById<TextView>(R.id.txtMessageTV)
        val noTV = alertDialog.findViewById<TextView>(R.id.noTV)
        val yesTV = alertDialog.findViewById<TextView>(R.id.yesTV)
        txtMessageTV.text = strMessage
        noTV.setOnClickListener { alertDialog.dismiss() }
        yesTV.setOnClickListener {
            if (isNetworkAvailable(mActivity)) {
                executeLogoutRequest()
            }
            else{
                showToast(mActivity, getString(R.string.internet_connection_error))
            }
        }
        alertDialog.show()
    }

    private fun executeLogoutRequest() {
        val headers: MutableMap<String, String> = HashMap()
        headers["Token"] = getAuthToken()

        showProgressDialog(mActivity)
        val call = RetrofitClient.apiInterface.LogOutDataRequest(headers)

        call.enqueue(object : Callback<LogOutModel> {
            override fun onFailure(call: Call<LogOutModel>, t: Throwable) {
                Log.e(TAG, t.message.toString())
                dismissProgressDialog()
            }
            @SuppressLint("ApplySharedPref")
            override fun onResponse(call: Call<LogOutModel>, response: Response<LogOutModel>) {
                Log.e(TAG, response.body().toString())
                dismissProgressDialog()
                var  mLogOutModel = response.body()
                if (mLogOutModel!!.status == 1) {
                    showToast(mActivity,mLogOutModel.message)
                    val preferences: SharedPreferences = AppPrefrences().getPreferences(mActivity)
                    val editor = preferences.edit()
                    editor.clear()
                    editor.commit()
                    editor.apply()
                    val intent = Intent(mActivity, SignInActivity::class.java)
                    startActivity(intent)
                    mActivity.let { ActivityCompat.finishAffinity(it) }
                }else {
                    showAlertDialog(mActivity, getString(R.string.internal_server_error))
                }
            }
        })
    }
}
package com.virtualcloset.app.activity

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.EditText
import android.widget.ImageView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.virtualcloset.app.Model.AddSearchModel
import com.virtualcloset.app.Model.SearchDataItem
import com.virtualcloset.app.R
import com.virtualcloset.app.Retrofit.RetrofitClient
import com.virtualcloset.app.adapter.SearchListAdapter
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.HashMap

import android.widget.TextView.OnEditorActionListener
import com.virtualcloset.app.Interface.SearchItemClickListener

class SearchActivity : BaseActivity() {
    @BindView(R.id.searchRV)
    lateinit var searchRV: RecyclerView

    @BindView(R.id.backRL)
    lateinit var backRL: ImageView

    @BindView(R.id.searchET)
    lateinit var searchET: EditText

    var searchListAdapter: SearchListAdapter? = null

    var AddSearchArrayList: ArrayList<SearchDataItem?>? = ArrayList()
    var Add_SearchArrayList: ArrayList<SearchDataItem?>? = ArrayList()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search)
        ButterKnife.bind(mActivity)

        if (isNetworkAvailable(mActivity)) {
            executeSearchRequest()
        } else {
            showToast(mActivity, getString(R.string.internet_connection_error))
        }

        searchET.setOnEditorActionListener(OnEditorActionListener { v, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                if (isNetworkAvailable(mActivity)) {
                    executeAddSearchRequest(searchET.text.toString())
                } else {
                    showToast(mActivity, getString(R.string.internet_connection_error))
                }
                return@OnEditorActionListener true
            }
            false
        })
    }
    private fun executeSearchRequest() {
        val headers: MutableMap<String, String> = HashMap()
        headers["Token"] = getAuthToken()

        val search: RequestBody = searchET.text.toString().trim()!!.toRequestBody("multipart/form-data".toMediaTypeOrNull())

        showProgressDialog(mActivity)
        val call = RetrofitClient.apiInterface.AddSearchDataRequest(headers,search)

        call.enqueue(object : Callback<AddSearchModel> {
            override fun onFailure(call: Call<AddSearchModel>, t: Throwable) {
                Log.e(TAG, t.message.toString())
                dismissProgressDialog()
            }
            override fun onResponse(call: Call<AddSearchModel>, response: Response<AddSearchModel>) {
                Log.e(TAG, response.body().toString())
                dismissProgressDialog()
                var mAddSearchModel = response.body()!!
                if (mAddSearchModel!!.status == 1) {
                    AddSearchArrayList=mAddSearchModel.data
                    Add_SearchArrayList= ArrayList(AddSearchArrayList!!.reversed())
                    setAdapter()
                }
                else if(mAddSearchModel!!.status == 0){
                  //  showToast(mActivity,mAddSearchModel!!.message)
                    if(mAddSearchModel.message.equals("Unautorized request, please login again.")){
                        var intent= Intent(mActivity,SignInActivity::class.java)
                        startActivity(intent)
                    }
                }
                else {
                    showAlertDialog(mActivity, getString(R.string.internal_server_error))
                }
            }
        })
    }
    private fun executeAddSearchRequest(toString: String) {
        val headers: MutableMap<String, String> = HashMap()
        headers["Token"] = getAuthToken()

        val search: RequestBody = toString.trim()!!.toRequestBody("multipart/form-data".toMediaTypeOrNull())

        showProgressDialog(mActivity)
        val call = RetrofitClient.apiInterface.AddSearchDataRequest(headers,search)

        call.enqueue(object : Callback<AddSearchModel> {
            override fun onFailure(call: Call<AddSearchModel>, t: Throwable) {
                Log.e(TAG, t.message.toString())
                dismissProgressDialog()
            }
            override fun onResponse(call: Call<AddSearchModel>, response: Response<AddSearchModel>) {
                Log.e(TAG, response.body().toString())
                dismissProgressDialog()
                var mAddSearchModel = response.body()!!
                if (mAddSearchModel!!.status == 1) {
                    if(mAddSearchModel.details != null){
                        val i = Intent(mActivity, SearchDetailActivity::class.java)
                        i.putExtra("search",toString)
                        startActivity(i)
                    }
                }
                else if(mAddSearchModel!!.status == 0){
                    showToast(mActivity,mAddSearchModel!!.message)
                }
                else {
                    showAlertDialog(mActivity, getString(R.string.internal_server_error))
                }
            }
        })
    }

    @OnClick(
        R.id.backRL
    )
    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.backRL -> onBackPressed()
        }
    }
    @SuppressLint("UseRequireInsteadOfGet")
    private fun setAdapter() {
        val layoutManager: RecyclerView.LayoutManager = LinearLayoutManager(mActivity, LinearLayoutManager.VERTICAL, false)
        searchRV.setLayoutManager(layoutManager)
        searchListAdapter = SearchListAdapter(mActivity,Add_SearchArrayList,mSearchItemClickListener)
        searchRV.setAdapter(searchListAdapter)

    }

    var mSearchItemClickListener : SearchItemClickListener = object : SearchItemClickListener {
        override fun onItemClickListenr(search: String?) {
            executeAddSearchRequest(search!!)
        }
    }
}
package com.virtualcloset.app.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.virtualcloset.app.R

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}